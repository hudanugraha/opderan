import { Component, OnInit, ViewChild } from '@angular/core';
import { IonTabs } from '@ionic/angular';
import { SqlService } from 'src/app/services/sql/sql.service';
import { TabsService } from 'src/app/services/tabs.service';

@Component({
  selector: 'app-tabs',
  templateUrl: './tabs.page.html',
  styleUrls: ['./tabs.page.scss'],
})
export class TabsPage implements OnInit {
  @ViewChild('myTabs') tabRef: IonTabs;
  index : any;
  
  constructor(private tabService: TabsService, private sql : SqlService){
    this.tabService.tabChange.subscribe((index:any) => {
      this.tabRef.select(index);
    });

    this.sql.createTable("CREATE TABLE IF NOT EXISTS pelanggan (id INTEGER PRIMARY KEY, nama TEXT NOT NULL, no_hp TEXT)");
    this.sql.createTable("CREATE TABLE IF NOT EXISTS produk (id INTEGER PRIMARY KEY, nama_produk TEXT NOT NULL, harga_pokok REAL NOT NULL, harga_jual REAL NOT NULL, kategori_id INTEGER NOT NULL, tampilkan TEXT)");
    this.sql.createTable("CREATE TABLE IF NOT EXISTS kategori (id INTEGER PRIMARY KEY, nama_kategori TEXT NOT NULL)");
    this.sql.createTable("CREATE TABLE IF NOT EXISTS orderan (id INTEGER PRIMARY KEY, lokasi TEXT NOT NULL, tanggal TEXT NOT NULL)");
    this.sql.createTable("CREATE TABLE IF NOT EXISTS orderandetail (id INTEGER PRIMARY KEY, id_orderan INTEGER NOT NULL, id_pelanggan INTEGER NOT NULL, id_produk INTEGER NOT NULL, qty INTEGER NOT NULL, harga REAL NOT NULL, harga_pokok REAL NOT NULL)");
    this.sql.createTable("CREATE TABLE IF NOT EXISTS statusbayarorderan (id INTEGER PRIMARY KEY, id_orderan INTEGER NOT NULL, id_pelanggan INTEGER NOT NULL, status_bayar TEXT DEFAULT \"Belum Bayar\" NOT NULL)");
    this.sql.createTable("CREATE TABLE IF NOT EXISTS appconfig (nama_config TEXT DEFAULT \"OPDERANAPP\" NOT NULL, value TEXT NOT NULL)");

  }

  ngOnInit() {
  }


}
