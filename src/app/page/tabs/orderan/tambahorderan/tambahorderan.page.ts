import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Orderan } from 'src/app/models/orderan.model';
import { GlobalService } from 'src/app/services/global/global.service';
import { OrderanService } from 'src/app/services/orderan/orderan.service';
import { StorageService } from 'src/app/services/storage/storage.service';


@Component({
  selector: 'app-tambahorderan',
  templateUrl: './tambahorderan.page.html',
  styleUrls: ['./tambahorderan.page.scss'],
})
export class TambahorderanPage implements OnInit { 

  formOrderan : FormGroup;
  lagiUpdate : boolean;
  isSubmitted = false;
  today = new Date().toISOString().slice(0, 10);
  idUpdate = 0;
  orderan: any=[];
  
  @ViewChild('lokasi') lokasi:any;


  constructor(
    private global : GlobalService,
    private route :ActivatedRoute,
    private orderanServis : OrderanService,
    public formBuilder: FormBuilder, 
    private router: Router,
    public storage : StorageService,
  ) { }


  ngOnInit() {    
    this.initForm();
    this.checkForUpdate();
  }

  initForm(orderan?: any) {
    let data: any = {
      lokasi : null,
      tanggal : this.today
    }
    if (orderan) {
      data = {
        lokasi : orderan.lokasi,
        tanggal : orderan.tanggal
      }
      this.today=orderan.tanggal;
    }
    this.formOrderan= this.formBuilder.group({
      lokasi: [data.lokasi, [Validators.required]],
      tanggal: [data.tanggal, [Validators.required]]
    });

  }

  setTanggal(event:any) {
    // var dateFormat = event.detail.value.split('T')[0]; 
  }

  ionViewDidEnter(){
    this.lokasi.setFocus();
  }

  checkForUpdate() {
    // karena halaman edit dan tambah sama, maka fungsi ini diperlukan utnuk mengecek apakah sekarang ini sedang edit atau tambah
    this.route.queryParams.subscribe(async(data) => {
      if (data?.['data']) {
        const orderan = JSON.parse(data['data']);
        this.lagiUpdate=true;
        this.idUpdate = orderan.id;
        this.orderan.lokasi=orderan.lokasi;
        this.orderan.tanggal=orderan.tanggal;
        this.initForm(this.orderan);
      } else {
        this.lagiUpdate=false;
      }
    })
  }

  get errorControl() {
    return this.formOrderan.controls;
  }

  async submitForm() {
    this.global.showLoader("Menyimpan...");
    this.isSubmitted = true;
    if (!this.formOrderan.valid) {
      console.log('Please provide all the required values!')
      this.global.hideLoader();
    } else {
      if (this.lagiUpdate) {
        await this.orderanServis.updateOrderan(this.idUpdate,this.formOrderan.value);
      } else {
        await this.orderanServis.tambahOrderan(this.formOrderan.value);      
      }
      this.router.navigate(['tabs/orderan']);        
      this.formOrderan.reset(); 
      this.global.hideLoader();
    }
  }
}
