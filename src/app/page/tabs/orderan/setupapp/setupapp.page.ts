import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { GlobalService } from 'src/app/services/global/global.service';

@Component({
  selector: 'app-setupapp',
  templateUrl: './setupapp.page.html',
  styleUrls: ['./setupapp.page.scss'],
})
export class SetupappPage implements OnInit {
  formSetup : FormGroup;
  isSubmitted = false;
  
  namatoko:any;

  constructor(    
    public formBuilder: FormBuilder,
    public global:GlobalService,
    private router: Router,
  ) {    
    this.getAppName();
  }

  ngOnInit() {
    this.initForm();
  }

  async getAppName() {
    this.namatoko=await this.global.getAppName();    
  }

  ionViewWillEnter() {
    this.getAppName();
  }

  async initForm(namaApp:string="") {
    // let data: any = {
    //   namatoko : this.namatoko
    // }
    // // if (pelanggan) {
    //   data = {
    //     nama_app : pelanggan.nama,
    //     no_hp : pelanggan.no_hp
    //   }
    // }

    this.formSetup = this.formBuilder.group({
      nama_toko: ["", [Validators.required]],
    });

  }

  async submitForm() {
    console.log(this.formSetup.value);
    this.global.showLoader("Menyimpan...");
    this.isSubmitted = true;
    if (!this.formSetup.valid) {
      console.log('Please provide all the required values!')
      this.global.hideLoader();
    } else {
      //console.log(this.formPelanggan.value);
      await this.global.setAppName(this.formSetup.value.nama_toko);
      this.formSetup.reset(); 
      this.global.hideLoader();
      this.router.navigate(['tabs/orderan']);
    }
  }

  get errorControl() {
    return this.formSetup.controls;
  }

}
