import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SetupappPageRoutingModule } from './setupapp-routing.module';

import { SetupappPage } from './setupapp.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SetupappPageRoutingModule,    
    ReactiveFormsModule
  ],
  declarations: [SetupappPage]
})
export class SetupappPageModule {}
