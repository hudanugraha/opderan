import { ComponentFixture, TestBed } from '@angular/core/testing';
import { SetupappPage } from './setupapp.page';

describe('SetupappPage', () => {
  let component: SetupappPage;
  let fixture: ComponentFixture<SetupappPage>;

  beforeEach(async(() => {
    fixture = TestBed.createComponent(SetupappPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
