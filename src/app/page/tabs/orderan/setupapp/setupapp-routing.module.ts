import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SetupappPage } from './setupapp.page';

const routes: Routes = [
  {
    path: '',
    component: SetupappPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SetupappPageRoutingModule {}
