import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { StatistikitemPage } from './statistikitem.page';

const routes: Routes = [
  {
    path: '',
    component: StatistikitemPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class StatistikitemPageRoutingModule {}
