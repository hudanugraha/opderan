import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { StatistikitemPageRoutingModule } from './statistikitem-routing.module';

import { StatistikitemPage } from './statistikitem.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    StatistikitemPageRoutingModule
  ],
  declarations: [StatistikitemPage]
})
export class StatistikitemPageModule {}
