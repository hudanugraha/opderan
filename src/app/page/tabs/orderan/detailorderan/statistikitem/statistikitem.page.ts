import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { KategoriService } from 'src/app/services/kategori/kategori.service';
import { OrderanService } from 'src/app/services/orderan/orderan.service';
import { ProdukService } from 'src/app/services/produk/produk.service';
import { SqlService } from 'src/app/services/sql/sql.service';

@Component({
  selector: 'app-statistikitem',
  templateUrl: './statistikitem.page.html',
  styleUrls: ['./statistikitem.page.scss'],
})
export class StatistikitemPage implements OnInit {

  DECIMAL_SEPARATOR=",";
  GROUP_SEPARATOR=".";
  orderan_id:number;
  kategoris : any = [];
  lokasi:string;
  tanggal:string;
  kategorisSub : Subscription; 
  produks : any = [];
  produksSub : Subscription; 
  jumlah_produk=0;
  isLoading=false;
  total_harga=0;
  harga_pokok=0;
  profit=0;
  totalStatistik:any=[];
  totalStatistikSub:Subscription;

  constructor(
    private route :ActivatedRoute,
    private orderanServis:OrderanService,
    private sql:SqlService,
    private kategoriServis : KategoriService,
    private produkServis:ProdukService
  ) { }

  ngOnInit() {
    this.initData();

    this.kategorisSub = this.kategoriServis.kategori.subscribe(kategori => {
      this.kategoris = kategori;
    })
    this.getKategori();

    //////////////
    this.produksSub = this.produkServis.produkstatistikitem.subscribe(produk => {
      this.isLoading=true;
      this.produks = produk;
      this.jumlah_produk=produk.length;
      this.isLoading=false;
    })
    this.getProduk();

    /////////////////
    this.totalStatistikSub= this.produkServis.totalstatistikitem.subscribe(total => {
      // this.isLoading=true;
      this.totalStatistik = total;
      // this.isLoading=false;
    })
    this.getTotalStatistik()
  }

  ngOnDestroy() {
    if (this.kategorisSub) this.kategorisSub.unsubscribe();
    if (this.produksSub) this.produksSub.unsubscribe();
    if (this.totalStatistikSub) this.totalStatistikSub.unsubscribe();
  }

  async initData() {
    /// SUBSCRIPTION NYA BELOM
    let id = this.route.snapshot.paramMap.get('orderan_id');
    this.orderan_id=Number(id);

    let orderan=await this.sql.getQuery("SELECT *,strftime('%d/%m/%Y', tanggal)  AS tanggal_teks FROM ORDERAN WHERE id='"+id+"' ");
    this.lokasi=orderan[0].lokasi;
    this.tanggal=orderan[0].tanggal_teks;
  }

  async getKategori() {
    await this.kategoriServis.getKategori();  
  }
  async getProduk() {
    await this.produkServis.getProdukStatistikItem(this.orderan_id);
  }
  async getTotalStatistik() {
    await this.produkServis.getTotalStatistikItem(this.orderan_id);
  }

  async filterKategori(event:any) {
    await this.produkServis.getProdukStatistikItem(this.orderan_id, event.detail.value);
    await this.produkServis.getTotalStatistikItem(this.orderan_id, event.detail.value);
  }

  hitungTotal() {
    let total_harga=0;
    this.produks.forEach(async (element:any, index:number, array=[]) => {
      
    }); 
  }

  formatRibuan(valString:any) {
    if (!valString) {
        return '';
    }
    let val = valString.toString();
    const parts = this.unFormatRibuan(val).split(this.DECIMAL_SEPARATOR);
    return parts[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, this.GROUP_SEPARATOR)
  };
  
  unFormatRibuan(val:any) {
    if (!val) {
        return '';
    }
    val = val.replace(/^0+/, '').replace(/\D/g,'');
    if (this.GROUP_SEPARATOR === ',') {
        return val.replace(/,/g, '');
    } else {
        return val.replace(/\./g, '');
    }
  }

}
