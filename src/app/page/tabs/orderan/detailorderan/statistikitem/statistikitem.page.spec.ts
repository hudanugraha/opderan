import { ComponentFixture, TestBed } from '@angular/core/testing';
import { StatistikitemPage } from './statistikitem.page';

describe('StatistikitemPage', () => {
  let component: StatistikitemPage;
  let fixture: ComponentFixture<StatistikitemPage>;

  beforeEach(async(() => {
    fixture = TestBed.createComponent(StatistikitemPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
