import { ComponentFixture, TestBed } from '@angular/core/testing';
import { EditorderanPage } from './editorderan.page';

describe('EditorderanPage', () => {
  let component: EditorderanPage;
  let fixture: ComponentFixture<EditorderanPage>;

  beforeEach(async(() => {
    fixture = TestBed.createComponent(EditorderanPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
