import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EditorderanPage } from './editorderan.page';

const routes: Routes = [
  {
    path: '',
    component: EditorderanPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EditorderanPageRoutingModule {}
