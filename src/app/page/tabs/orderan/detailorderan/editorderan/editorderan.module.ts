import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { EditorderanPageRoutingModule } from './editorderan-routing.module';

import { EditorderanPage } from './editorderan.page';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome'
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    FontAwesomeModule,
    EditorderanPageRoutingModule
  ],
  declarations: [EditorderanPage]
})
export class EditorderanPageModule {}
