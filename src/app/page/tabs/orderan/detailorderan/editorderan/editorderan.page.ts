import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { OrderanService } from 'src/app/services/orderan/orderan.service';
import { PelangganService } from 'src/app/services/pelanggan/pelanggan.service';
import { SqlService } from 'src/app/services/sql/sql.service';

@Component({
  selector: 'app-editorderan',
  templateUrl: './editorderan.page.html',
  styleUrls: ['./editorderan.page.scss'],
})
export class EditorderanPage implements OnInit {

  DECIMAL_SEPARATOR=",";
  GROUP_SEPARATOR=".";
  orderan_id:number;
  pelanggan_id:number;
  isLoading = false;
  pelanggan : any = [];
  total_harga =0;
  jumlah_item =0;
  pelangganOrderProdukSub : Subscription;  
  produks : any = [];
  pelangganOrderSub : Subscription;
  pelangganOrder: any=[];

  constructor(
    private route :ActivatedRoute,
    private sql : SqlService,
    private pelangganServis : PelangganService,  
    private orderanServis : OrderanService,  
    private router: Router,
  ) { }

  async ngOnInit() {
    let id_orderan = this.route.snapshot.paramMap.get('id_orderan');
    this.orderan_id=Number(id_orderan);
    
    // get detail pelanggan
    let id_pelanggan = this.route.snapshot.paramMap.get('id_pelanggan');
    this.pelanggan_id=Number(id_pelanggan);
    this.pelanggan=await this.pelangganServis.getDetailPelanggan(this.pelanggan_id);

    this.isLoading = true;
    this.pelangganOrderProdukSub = this.orderanServis.produkorderanpelanggan.subscribe(produk => {
      this.isLoading = true;
      this.produks = produk;
      this.isLoading=false;
      this.hitungTotal();
    
    })    
    await this.getProdukOrderan();

    ////////////// utk halaman utama detailorderan
    this.pelangganOrderSub = this.orderanServis.pelangganorder.subscribe(pelanggan => {
      this.isLoading = true;
      this.pelangganOrder = pelanggan;
      this.isLoading=false;
      this.hitungTotal();    
      // }
    });

    await this.getPelangganOrder();
    
    this.isLoading=false;
  }

  async getProdukOrderan() {
    // this.isLoading = true;
    await this.orderanServis.getProdukOrderanPelanggan(this.orderan_id, this.pelanggan_id);     
    // this.isLoading=false;
  }

  ngOnDestroy() {
    if (this.pelangganOrderProdukSub) this.pelangganOrderProdukSub.unsubscribe(); 
    if (this.pelangganOrderSub) this.pelangganOrderSub.unsubscribe();
  }

  async tambahQty(produk:any) {
    await this.orderanServis.tambahQty(produk, this.orderan_id,this.pelanggan_id);
    this.getProdukOrderan();
    this.hitungTotal();
  }

  async kurangQty(produk:any) {
    await this.orderanServis.kurangQty(produk, this.orderan_id,this.pelanggan_id);
    this.getProdukOrderan();
    this.hitungTotal();
  }

  navigateToPlus() {
    this.router.navigate(["tabs","orderan","tambahorderanproduk",this.orderan_id, this.pelanggan_id]);
  }

  async hitungTotal() {
    let item = this.produks.filter((x : any) => x.qty  > 0);
    let total=0; let totalitem=0;
    item.forEach((element:any) => {
      totalitem+=element.qty;
      total+=element.qty*element.harga_jual;
    })
    this.jumlah_item=totalitem;
    this.total_harga=this.formatRibuan(total);

    this.getPelangganOrder();
  }

  // untuk merefresh saat back
  async getPelangganOrder() {
    // this.isLoading = true;
    await this.orderanServis.getPelangganOrder(this.orderan_id);     
    // this.isLoading=false;
  }

  formatRibuan(valString:any) {
    if (!valString) {
        return '';
    }
    let val = valString.toString();
    const parts = this.unFormatRibuan(val).split(this.DECIMAL_SEPARATOR);
    return parts[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, this.GROUP_SEPARATOR)
  
  };
  
  unFormatRibuan(val:any) {
    if (!val) {
        return '';
    }
    val = val.replace(/^0+/, '').replace(/\D/g,'');
    if (this.GROUP_SEPARATOR === ',') {
        return val.replace(/,/g, '');
    } else {
        return val.replace(/\./g, '');
    }
  }

}
