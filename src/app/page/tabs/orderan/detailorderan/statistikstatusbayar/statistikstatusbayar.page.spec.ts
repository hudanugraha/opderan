import { ComponentFixture, TestBed } from '@angular/core/testing';
import { StatistikstatusbayarPage } from './statistikstatusbayar.page';

describe('StatistikstatusbayarPage', () => {
  let component: StatistikstatusbayarPage;
  let fixture: ComponentFixture<StatistikstatusbayarPage>;

  beforeEach(async(() => {
    fixture = TestBed.createComponent(StatistikstatusbayarPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
