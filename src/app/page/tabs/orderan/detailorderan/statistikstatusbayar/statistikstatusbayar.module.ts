import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { StatistikstatusbayarPageRoutingModule } from './statistikstatusbayar-routing.module';

import { StatistikstatusbayarPage } from './statistikstatusbayar.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    StatistikstatusbayarPageRoutingModule
  ],
  declarations: [StatistikstatusbayarPage]
})
export class StatistikstatusbayarPageModule {}
