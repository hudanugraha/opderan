import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { GlobalService } from 'src/app/services/global/global.service';
import { OrderanService } from 'src/app/services/orderan/orderan.service';
import { SqlService } from 'src/app/services/sql/sql.service';

@Component({
  selector: 'app-statistikstatusbayar',
  templateUrl: './statistikstatusbayar.page.html',
  styleUrls: ['./statistikstatusbayar.page.scss'],
})
export class StatistikstatusbayarPage implements OnInit, OnDestroy {
  
  DECIMAL_SEPARATOR=",";
  GROUP_SEPARATOR=".";
  orderan_id:number;
  lokasi:string;
  tanggal:string;
  jumlahStatusBayarSub : Subscription;
  subtotalStatusBayarSub: Subscription;
  
  arrStatusBayar: any=[];
  arrJumlah:any=[];
  arrSubtotal:any=[];
  arrSubtotal_teks:any=[];
  isLoading=false;
  jumlah_pelanggan=0;
  totalsemua="";

  constructor(    
    private route :ActivatedRoute,
    private sql:SqlService,
    private global:GlobalService,
    private orderanServis:OrderanService
  ) { }

  ngOnInit() {
    this.initData();
  }

  async initData() {
    /// SUBSCRIPTION NYA BELOM
    let id = this.route.snapshot.paramMap.get('orderan_id');
    this.orderan_id=Number(id);

    let orderan=await this.sql.getQuery("SELECT *,strftime('%d/%m/%Y', tanggal)  AS tanggal_teks FROM ORDERAN WHERE id='"+id+"' ");
    this.lokasi=orderan[0].lokasi;
    this.tanggal=orderan[0].tanggal_teks;

    //  list status2nya
    this.arrStatusBayar=await this.global.getArrStatusBayar();    

    // listjumlahnya
    this.jumlahStatusBayarSub = this.orderanServis.statistikstatusbayarjumlah.subscribe(jumlahstatusbayar=> {
      this.isLoading = true;      
      this.arrJumlah = jumlahstatusbayar;
      this.isLoading=false;            
    });

    // list subtotal
    this.subtotalStatusBayarSub = this.orderanServis.statistikstatusbayarsubtotal.subscribe(subtotalstatusbayar=> {
      this.isLoading = true;      
      this.arrSubtotal = subtotalstatusbayar;
      // tambahkan format ribuan
      let total=0;
      for (let i=0; i<this.arrSubtotal.length; i++) {
        this.arrSubtotal_teks.push(this.formatRibuan(this.arrSubtotal[i]));
        total=total+this.arrSubtotal[i];
        if (this.arrSubtotal[i]>0) {
          this.jumlah_pelanggan++;
        }
      }
      this.totalsemua=this.formatRibuan(total);
      this.isLoading=false;            
    });


    this.getStatistikPelangganOrder();           
  }

  async ngOnDestroy() {
    if (this.jumlahStatusBayarSub) this.jumlahStatusBayarSub.unsubscribe();
    if (this.subtotalStatusBayarSub) this.subtotalStatusBayarSub.unsubscribe();
  }

  async getStatistikPelangganOrder() {
    // this.isLoading = true;
    // await this.orderanServis.getPelangganOrder(this.orderan_id);     

    // let arr:any=[];  
    // let arrtotalbayar:any=[];      
    // this.pelangganOrder.forEach(async (element:any, index:number, array=[]) => { 
    //     if (element.status_bayar==null) {
    //       element.status_bayar="Belum Bayar";
    //     }

    //     if (!arr[element.status_bayar]) {
    //       arr[element.status_bayar]=1;
    //     } else {
    //       arr[element.status_bayar]++;
    //     }
        

    //     // get produk2 dari pemesanan ini
    //     // let tipeJoin="LEFT";
    //     // let qstatusbayar="";
    //     // let qcaribelumbayar="";
    //     // if (element.status_bayar=="Belum Bayar") {
    //     //     qcaribelumbayar=" AND (c.status_bayar IS NULL OR c.status_bayar='Belum Bayar') ";
    //     // } else {
    //     //     qstatusbayar=" AND status_bayar='"+element.status_bayar+"' ";
    //     //     tipeJoin="INNER";
    //     // }
    //     // let q="SELECT a.id_produk, a.qty, a.harga FROM orderandetail a "+
    //     //        tipeJoin+" JOIN (SELECT id_pelanggan, status_bayar FROM statusbayarorderan WHERE id_orderan='"+this.orderan_id+"' "+qstatusbayar+") c ON a.id_pelanggan=c.id_pelanggan "+
    //     //       "WHERE a.id_orderan='"+this.orderan_id+"' AND a.id_pelanggan='"+element.id_pelanggan+"' "+qcaribelumbayar;
    //     // let produkOrderanPelangganIni:any=await this.sql.getQuery(q);
    //     // let arrtotal:any=[];
    //     // produkOrderanPelangganIni.forEach(async (el:any, index:number, array=[]) => { 
         
    //     // });
    // });

    // console.log(this.statusBayar);

    // // this.arrStatusBayar.jumlahstatus=arr;
    // // console.log(this.arrStatusBayar);
    // this.isLoading=false;

    // this.statusBayar.forEach(async (element:any, index:number, array=[]) => { 
    //   let pelanggan=await this.orderanServis.getPelangganOrder(this.orderan_id, "",element.status);  
    //   this.statusBayar[index].jumlahOrang=pelanggan.length;
    // });

    await this.orderanServis.getStatistikStatusBayar(this.orderan_id);

  }

  formatRibuan(valString:any) {
    if (!valString) {
        return '';
    }
    let val = valString.toString();
    const parts = this.unFormatRibuan(val).split(this.DECIMAL_SEPARATOR);
    return parts[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, this.GROUP_SEPARATOR)
  };
  
  unFormatRibuan(val:any) {
    if (!val) {
        return '';
    }
    val = val.replace(/^0+/, '').replace(/\D/g,'');
    if (this.GROUP_SEPARATOR === ',') {
        return val.replace(/,/g, '');
    } else {
        return val.replace(/\./g, '');
    }
  }

}

