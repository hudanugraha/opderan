import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { StatistikstatusbayarPage } from './statistikstatusbayar.page';

const routes: Routes = [
  {
    path: '',
    component: StatistikstatusbayarPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class StatistikstatusbayarPageRoutingModule {}
