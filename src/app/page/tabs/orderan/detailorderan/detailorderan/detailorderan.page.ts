import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';
import { NavController } from '@ionic/angular';
import { Subscription } from 'rxjs';
import { Orderan } from 'src/app/models/orderan.model';
import { GlobalService } from 'src/app/services/global/global.service';
import { KategoriService } from 'src/app/services/kategori/kategori.service';
import { OrderanService } from 'src/app/services/orderan/orderan.service';
import { ProdukService } from 'src/app/services/produk/produk.service';
import { SqlService } from 'src/app/services/sql/sql.service';
import { StorageService } from 'src/app/services/storage/storage.service';
import { PopoverController } from '@ionic/angular';  
import { StatusbayarComponent } from 'src/app/component/statusbayar/statusbayar.component';
// import { StatusbayarComponent } from 'src/app/component/statusbayar/statusbayar.component';


@Component({
  selector: 'app-detailorderan',
  templateUrl: './detailorderan.page.html',
  styleUrls: ['./detailorderan.page.scss'],
})
export class DetailorderanPage implements OnInit, OnDestroy {
  roleMsg = '';
  DECIMAL_SEPARATOR=",";
  GROUP_SEPARATOR=".";
  isLoading = false; 
  orderan : any=[];
  lokasi:string;
  tanggal:string;
  orderan_id:number;
  kategoris : any = [];
  pelangganorder: any = [];
  jumlah_pelanggan=0;
  pelangganOrderSub : Subscription;
  pelangganOrder: any=[];
  statusBayar:any=[];
  isOpen : any = [];
  queryCari = "";
  filterStatus="";

  constructor(
    private route :ActivatedRoute,
    private router: Router,
    public navCtrl: NavController,
    private storage : StorageService,
    private sql : SqlService,
    private produkServis : ProdukService,
    private kategoriServis : KategoriService,
    private orderanServis : OrderanService,
    private global : GlobalService,
    public popoverController: PopoverController  
  ) { }

  ngOnInit() {
    // inisiasi
    this.initData();
    this.getStatusBayar();
  }

  async ubahStatusBayar(e: Event, index:number, id_pelanggan:number) {
    const popover = await this.popoverController.create({
      component: StatusbayarComponent,
      event: e,
    });

    popover.onDidDismiss().then((dataReturned) => {
      if ((dataReturned !== null)&&(dataReturned.data != undefined)) {
        // dataReturned.data;
        console.log("RETURNED DATA",dataReturned.data);
        let data=dataReturned.data;
        this.pelangganOrder[index].status_bayar=data.status;
        this.pelangganOrder[index].warna_status_bayar=data.warna;
        // save
        this.orderanServis.saveStatusBayar(this.orderan_id, id_pelanggan, data.status);
        this.global.showToast("Berhasil ubah status bayar", 'success','bottom',1500);

      }
    })
    await popover.present();
    // const { role } = await popover.onDidDismiss();
    // console.log("Popover dismissed with role:", role);
  }

  async getStatusBayar() {
    this.statusBayar=await this.global.getArrStatusBayar();
  }

  async initData() {
      // let q="DROP TABLE orderandetail";
      // this.sql.setQuery(q);
      // let q2="DROP TABLE statusbayarorderan";
      // this.sql.setQuery(q2);

      let id = this.route.snapshot.paramMap.get('id');
      this.orderan_id=Number(id);
      
      let orderan=await this.sql.getQuery("SELECT *,strftime('%d/%m/%Y', tanggal)  AS tanggal_teks FROM ORDERAN WHERE id='"+id+"' ");
      this.lokasi=orderan[0].lokasi;
      this.tanggal=orderan[0].tanggal_teks;
      this.kategoris=await this.sql.getQuery("SELECT * FROM kategori");

      this.pelangganOrderSub = this.orderanServis.pelangganorder.subscribe(pelanggan => {
        this.isLoading = true;
        this.pelangganOrder = pelanggan;
        this.jumlah_pelanggan=this.pelangganOrder.length;
        this.isLoading=false;
      })
      this.getPelangganOrder();      
  }

  ngOnDestroy() {
    if (this.pelangganOrderSub) this.pelangganOrderSub.unsubscribe();     
  }

  ionViewWillEnter() {
    this.getPelangganOrder();      
  }

  async getPelangganOrder() {
    this.isLoading = true;
    await this.orderanServis.getPelangganOrder(this.orderan_id);     
    this.isLoading=false;
  }

  async filterKategori(event:any, tipe:string) {
    if (tipe=='query') {
      this.queryCari= event.detail.value;
    }
    if (tipe=='status') {
      this.filterStatus= event.detail.value;
    }

    if ((this.queryCari !="") || (this.filterStatus!="")) {
      await this.orderanServis.getPelangganOrder(this.orderan_id, this.queryCari, this.filterStatus);    
    } else {
      this.getPelangganOrder();
    }
  }

  hapusProduk(id_pelanggan:number, id_produk:number) {
    this.global.showAlert(
      "Yakin ingin menghapus?",
      "Konfirmasi",
      [
              {
                text : "Tidak",
                role : 'cancel',
                handler: () => {
                  console.log('cancel');
                  return;
                }
              },
              {
                text : "Ya",
                handler : async () => {
                  this.global.showLoader("Menghapus...");
                  await this.orderanServis.hapusDetailProdukPelanggan(this.orderan_id,id_pelanggan, id_produk);
                  this.global.hideLoader();
                }
              }
            ]
    )
  }

  pilihStatusBayar(id_pelanggan:number) {
    this.isOpen[id_pelanggan]=true;
  }

  formatRibuan(valString:any) {
    if (!valString) {
        return '';
    }
    let val = valString.toString();
    const parts = this.unFormatRibuan(val).split(this.DECIMAL_SEPARATOR);
    return parts[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, this.GROUP_SEPARATOR)
  
  };
  
  unFormatRibuan(val:any) {
    if (!val) {
        return '';
    }
    val = val.replace(/^0+/, '').replace(/\D/g,'');
    if (this.GROUP_SEPARATOR === ',') {
        return val.replace(/,/g, '');
    } else {
        return val.replace(/\./g, '');
    }
  }

  

}
