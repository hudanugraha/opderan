import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DetailorderanPageRoutingModule } from './detailorderan-routing.module';

import { DetailorderanPage } from './detailorderan.page';
import { StatusbayarComponent } from 'src/app/component/statusbayar/statusbayar.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DetailorderanPageRoutingModule
  ],
  declarations: [DetailorderanPage, StatusbayarComponent]
})
export class DetailorderanPageModule {}
