import { ComponentFixture, TestBed } from '@angular/core/testing';
import { DetailorderanPage } from './detailorderan.page';

describe('DetailorderanPage', () => {
  let component: DetailorderanPage;
  let fixture: ComponentFixture<DetailorderanPage>;

  beforeEach(async(() => {
    fixture = TestBed.createComponent(DetailorderanPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
