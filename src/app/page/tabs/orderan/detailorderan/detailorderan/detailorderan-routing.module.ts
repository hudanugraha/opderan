import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DetailorderanPage } from './detailorderan.page';

const routes: Routes = [
  {
    path: '',
    component: DetailorderanPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DetailorderanPageRoutingModule {}
