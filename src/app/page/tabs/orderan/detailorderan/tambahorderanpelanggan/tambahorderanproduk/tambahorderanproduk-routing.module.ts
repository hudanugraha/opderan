import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TambahorderanprodukPage } from './tambahorderanproduk.page';

const routes: Routes = [
  {
    path: '',
    component: TambahorderanprodukPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TambahorderanprodukPageRoutingModule {}
