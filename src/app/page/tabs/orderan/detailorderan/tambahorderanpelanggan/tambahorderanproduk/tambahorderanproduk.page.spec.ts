import { ComponentFixture, TestBed } from '@angular/core/testing';
import { TambahorderanprodukPage } from './tambahorderanproduk.page';

describe('TambahorderanprodukPage', () => {
  let component: TambahorderanprodukPage;
  let fixture: ComponentFixture<TambahorderanprodukPage>;

  beforeEach(async(() => {
    fixture = TestBed.createComponent(TambahorderanprodukPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
