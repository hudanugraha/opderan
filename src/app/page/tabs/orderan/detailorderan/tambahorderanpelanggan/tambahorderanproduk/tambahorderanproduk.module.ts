import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TambahorderanprodukPageRoutingModule } from './tambahorderanproduk-routing.module';

import { TambahorderanprodukPage } from './tambahorderanproduk.page';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    FontAwesomeModule,
    TambahorderanprodukPageRoutingModule
  ],
  declarations: [TambahorderanprodukPage]
})
export class TambahorderanprodukPageModule {}
