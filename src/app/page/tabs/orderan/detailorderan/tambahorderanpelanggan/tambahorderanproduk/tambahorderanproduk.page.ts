import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NavController } from '@ionic/angular';
import { Subscription } from 'rxjs';
import { Produk } from 'src/app/models/produk.model';
import { OrderanService } from 'src/app/services/orderan/orderan.service';
import { ProdukService } from 'src/app/services/produk/produk.service';
import { SqlService } from 'src/app/services/sql/sql.service';
import { StorageService } from 'src/app/services/storage/storage.service';

@Component({
  selector: 'app-tambahorderanproduk',
  templateUrl: './tambahorderanproduk.page.html',
  styleUrls: ['./tambahorderanproduk.page.scss'],
})
export class TambahorderanprodukPage implements OnInit, OnDestroy {

  DECIMAL_SEPARATOR=",";
  GROUP_SEPARATOR=".";
  produks : any = [];
  produksSub : Subscription; 
  orderandetail: any = [];
  orderanDetailSub : Subscription;
  isLoading : boolean;
  queryCari = "";
  jumlah_item =0;
  total_harga=0;
  nama_pelanggan="";
  pelanggan_id=0;
  orderan_id=0;
  btn_disabled=true;
  isKeyboardHide=true;
  pelangganOrderSub : Subscription;
  pelangganOrder: any=[];
  
  constructor(
    private produkServis : ProdukService,
    private storage : StorageService,
    private route :ActivatedRoute,
    private orderanServis : OrderanService,
    private navCtrl : NavController,
    private sql : SqlService,
    private router: Router,
  ) { 
 }

  async ngOnInit() {
    this.orderan_id = Number(this.route.snapshot.paramMap.get('id_orderan'));
    this.pelanggan_id= Number(this.route.snapshot.paramMap.get('id_pelanggan'));
    
    this.getProduk();


    ////////////// utk halaman utama detailorderan
    this.pelangganOrderSub = this.orderanServis.pelangganorder.subscribe(pelanggan => {
      this.isLoading = true;
      this.pelangganOrder = pelanggan;
      this.isLoading=false;
      // }
    });
  }

  ngOnDestroy() {
    if (this.produksSub) this.produksSub.unsubscribe();
    if (this.pelangganOrderSub) this.pelangganOrderSub.unsubscribe();
  }

  ionViewWillEnter() {
    console.log("IONVIEWWILLENTER PRODUKTAMBAHORDERAN");
    this.getProduk();
  }

  async getProduk(qCari:string="") {
    this.isLoading = true;
    // await this.produkServis.getProduk(true);  
    let q="SELECT a.id, a.nama_produk, a.harga_jual, a.harga_pokok, replace(printf(\"%,d\", a.harga_jual), ',', '.') AS harga_jual_teks, b.qty, b.harga FROM produk a "+ 
        "LEFT JOIN ("+
        "SELECT * FROM orderandetail "+ 
            "WHERE id_pelanggan='"+this.pelanggan_id+"' AND id_orderan='"+this.orderan_id+"'"+
        ") b "+ 
        " ON a.id=b.id_produk WHERE (a.tampilkan='1' OR a.tampilkan='true') "+qCari;
    let allProduk=await this.sql.getQuery(q);
    this.produks=allProduk;
    this.hitungTotal();
    this.isLoading=false;
  }
  async cariProduk(event:any) {
    
    let q="";
    if (event.detail.value !="") {
      let q=" AND a.nama_produk LIKE '%"+event.detail.value+"%'";       
      this.getProduk(q);     
      this.queryCari=event.detail.value;
    } else {
      this.getProduk();
    }
    
    
  }

  async tambahQty(produk:Produk, index:number) { 
    // cek if exist record
    let rec=await this.sql.getQuery("SELECT * FROM orderandetail WHERE  id_orderan='"+this.orderan_id+"' AND id_pelanggan='"+this.pelanggan_id+"' AND id_produk='"+produk.id+"' ");
    let q="";
    if (rec.length==0) {
      // insert
      q="INSERT INTO orderandetail (id_orderan,id_pelanggan,id_produk,qty,harga,harga_pokok) VALUES ('"+this.orderan_id+"', '"+this.pelanggan_id+"','"+produk.id+"', 1,'"+produk.harga_jual+"', '"+produk.harga_pokok+"')";        
    } else {
      //tambahkan dan update
      let qty=rec[0].qty;
      qty++;
      
      q="UPDATE orderandetail SET qty='"+qty+"' WHERE id_orderan='"+this.orderan_id+"' AND id_pelanggan='"+this.pelanggan_id+"'  AND id_produk='"+produk.id+"'";      
    }
    this.sql.setQuery(q);

    let qcari=" AND a.nama_produk LIKE '%"+this.queryCari+"%'";  
    await this.getProduk(qcari);
    this.hitungTotal();
    // this.sql.setQuery("DELETE FROM orderandetail");
  }  

  async kurangQty(produk:Produk,index:number) {
    let rec=await this.sql.getQuery("SELECT * FROM orderandetail WHERE  id_orderan='"+this.orderan_id+"' AND id_pelanggan='"+this.pelanggan_id+"' AND id_produk='"+produk.id+"' ");
    let q="";
    if (rec.length==0) {
      // belum ada data, abaikan saja karena ini pengurangan
    } else {
      // get qty sebelumnya, kurangi jika qty>0
      let qty=rec[0].qty;
      if (qty>0) {  
        qty--;
        q="UPDATE orderandetail SET qty='"+qty+"' WHERE id_orderan='"+this.orderan_id+"' AND id_pelanggan='"+this.pelanggan_id+"'  AND id_produk='"+produk.id+"'";   
        this.sql.setQuery(q);

        if (qty==0) {
          // hapus
          q="DELETE FROM orderandetail  WHERE id_orderan='"+this.orderan_id+"' AND id_pelanggan='"+this.pelanggan_id+"'  AND id_produk='"+produk.id+"'";   
          this.sql.setQuery(q); 
        }

        let qcari="";
        if (this.queryCari!="") { 
          qcari=" AND a.nama_produk LIKE '%"+this.queryCari+"%'";  
        } else {
          qcari="";
        }
        await this.getProduk(qcari);
      }       
      this.hitungTotal();
    }    
  }

  hitungTotal() {
    let item = this.produks.filter((x : any) => x.qty  > 0);
    let total=0; let totalitem=0;
    item.forEach((element:any) => {
      totalitem+=element.qty;
      total+=element.qty*element.harga_jual;
    })
    this.jumlah_item=totalitem;
    this.total_harga=this.formatRibuan(total);
    if (totalitem>0) {
      this.btn_disabled = false; 
    } else {
      this.btn_disabled = true;
    }
    // this.orderanServis.simpanOrderanProduk(this.id_orderan, 2);
    this.orderanServis.getPelangganOrder(this.orderan_id);
  }

  simpanItem() {
    this.router.navigate(['tabs','orderan', 'detailorderan',this.orderan_id]);
  }

  formatRibuan(valString:any) {
    if (!valString) {
        return '';
    }
    let val = valString.toString();
    const parts = this.unFormatRibuan(val).split(this.DECIMAL_SEPARATOR);
    return parts[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, this.GROUP_SEPARATOR)
  
  };
  
  unFormatRibuan(val:any) {
    if (!val) {
        return '';
    }
    val = val.replace(/^0+/, '').replace(/\D/g,'');
    if (this.GROUP_SEPARATOR === ',') {
        return val.replace(/,/g, '');
    } else {
        return val.replace(/\./g, '');
    }
  }

}
