import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TambahorderanpelangganPageRoutingModule } from './tambahorderanpelanggan-routing.module';

import { TambahorderanpelangganPage } from './tambahorderanpelanggan.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TambahorderanpelangganPageRoutingModule
  ],
  declarations: [TambahorderanpelangganPage]
})
export class TambahorderanpelangganPageModule {}
