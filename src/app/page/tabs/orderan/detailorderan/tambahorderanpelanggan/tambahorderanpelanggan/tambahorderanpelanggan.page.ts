import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, NavigationExtras, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { Pelanggan } from 'src/app/models/pelanggan.model';
import { GlobalService } from 'src/app/services/global/global.service';
import { PelangganService } from 'src/app/services/pelanggan/pelanggan.service';
import { StorageService } from 'src/app/services/storage/storage.service';

@Component({
  selector: 'app-tambahorderanpelanggan',
  templateUrl: './tambahorderanpelanggan.page.html',
  styleUrls: ['./tambahorderanpelanggan.page.scss'],
})
export class TambahorderanpelangganPage implements OnInit, OnDestroy {
  
  pelanggans : any = [];
  pelanggansSub : Subscription;
  queryCari : string = "";
  isLoading = false;
  orderan_id:number;

  constructor(
    private route :ActivatedRoute,
    private pelangganService : PelangganService,
    private router: Router,
    private storage :StorageService,
    private global:GlobalService,
  ) { }

  ngOnInit() {
    
    let id = this.route.snapshot.paramMap.get('id');
    this.orderan_id=Number(id);
    this.pelanggansSub = this.pelangganService.pelanggansOrderan.subscribe(pelanggan => {
      this.pelanggans = pelanggan;
    })
    this.getPelanggan();

  }

  ngOnDestroy() {
    if (this.pelanggansSub) this.pelanggansSub.unsubscribe();
  }

  async getPelanggan() {
    this.isLoading = true;
    await this.pelangganService.getPelangganOrderan(this.orderan_id,this.queryCari);     
    this.isLoading=false;
  }

  editPelanggan(pelanggan:Pelanggan) {
    pelanggan['isFromOrderanPage']=true; 
    const navData: NavigationExtras = {
      queryParams: {
        data: JSON.stringify(pelanggan)
      }
    }
    this.router.navigate(['tabs','pelanggan', 'tambahpelanggan'],navData);
  }

  async cariPelanggan(event: any) {
    // this.queryCari = event.detail.value.toLowerCase();
    // const temp = await this.storage.get("pelanggan");
    // let allPelanggan: Pelanggan [] =  JSON.parse(temp.value);
    // console.log("allPelanggan", allPelanggan);
    // if (this.queryCari.length > 0) {
     
    //   setTimeout(async() => {
    //     this.pelanggans = allPelanggan.filter((element: any) => { 
    //       return element.nama.toLowerCase().includes(this.queryCari);
    //     })        
    //   });
    // } else {
    //   // console.log("TIDAK ADA QUERY");
    //   this.getPelanggan();
    // }    
    this.queryCari = event.detail.value.toLowerCase();
    this.global.showLoader("Mencari...");
    if (this.queryCari.length > 0) { 
      this.pelangganService.cariPelanggan(this.queryCari);
    } else {
      this.getPelanggan();
    }
    this.global.hideLoader();
  }

  navigateToTambahProduk(data:Pelanggan) {
    // const navData: NavigationExtras = {
    //   queryParams: {
    //     data: JSON.stringify(data)
    //   }
    // }
    this.router.navigate(["tabs","orderan","tambahorderanproduk",this.orderan_id, data.id]);
  }

}
