import { ComponentFixture, TestBed } from '@angular/core/testing';
import { TambahorderanpelangganPage } from './tambahorderanpelanggan.page';

describe('TambahorderanpelangganPage', () => {
  let component: TambahorderanpelangganPage;
  let fixture: ComponentFixture<TambahorderanpelangganPage>;

  beforeEach(async(() => {
    fixture = TestBed.createComponent(TambahorderanpelangganPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
