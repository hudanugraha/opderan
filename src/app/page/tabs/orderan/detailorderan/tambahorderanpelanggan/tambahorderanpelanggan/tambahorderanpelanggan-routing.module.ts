import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TambahorderanpelangganPage } from './tambahorderanpelanggan.page';

const routes: Routes = [
  {
    path: '',
    component: TambahorderanpelangganPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TambahorderanpelangganPageRoutingModule {}
