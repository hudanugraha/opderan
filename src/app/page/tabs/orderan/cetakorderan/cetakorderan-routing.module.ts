import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CetakorderanPage } from './cetakorderan.page';

const routes: Routes = [
  {
    path: '',
    component: CetakorderanPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CetakorderanPageRoutingModule {}
