import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { GlobalService } from 'src/app/services/global/global.service';
import { OrderanService } from 'src/app/services/orderan/orderan.service';
import { SqlService } from 'src/app/services/sql/sql.service';

@Component({
  selector: 'app-cetakorderan',
  templateUrl: './cetakorderan.page.html',
  styleUrls: ['./cetakorderan.page.scss'],
})
export class CetakorderanPage implements OnInit {

  showStatusBayar=false; 
  namaApp="";
  DECIMAL_SEPARATOR=",";
  GROUP_SEPARATOR=".";
  orderan_id=0;
  pelanggan_id=0;
  lokasi:string;
  tanggal:string;
  pelangganOrderSub : Subscription;
  pelangganOrder: any=[];
  isLoading=false;
  showOpsiFilterStatusBayar=true;
  

  constructor(
    private global : GlobalService,
    private route :ActivatedRoute,
    private sql : SqlService,
    private orderanServis : OrderanService,

  ) { }

  ngOnInit() {
    this.initData();
  }

  async initData() {
    this.namaApp=await this.global.getAppName();
    
    let id = this.route.snapshot.paramMap.get('orderan_id');
    this.orderan_id=Number(id);

    let id_pelanggan = this.route.snapshot.paramMap.get('pelanggan_id');
    this.pelanggan_id=Number(id_pelanggan);
  
    let orderan=await this.sql.getQuery("SELECT *,strftime('%d/%m/%Y', tanggal)  AS tanggal_teks FROM ORDERAN WHERE id='"+id+"' ");
    this.lokasi=orderan[0].lokasi;
    this.tanggal=orderan[0].tanggal_teks;

    this.pelangganOrderSub = this.orderanServis.pelangganorder.subscribe(pelanggan => {
      this.isLoading = true;
      this.pelangganOrder = pelanggan;
      // this.jumlah_pelanggan=this.pelangganOrder.length;

      this.isLoading=false;
    })
    this.getPelangganOrder();  
  }

  showHideStatusBayar() {
    if (!this.showStatusBayar) {
      this.showStatusBayar=true;
    } else {
      this.showStatusBayar=false;
    }
  }

  async getPelangganOrder() {
    this.isLoading = true;
    await this.orderanServis.getPelangganOrder(this.orderan_id,"","",this.pelanggan_id);
    this.showStatusBayar=false;
    if (this.pelanggan_id!=0) {
      this.showOpsiFilterStatusBayar=false;         
    }
    this.isLoading=false;
  }

}
