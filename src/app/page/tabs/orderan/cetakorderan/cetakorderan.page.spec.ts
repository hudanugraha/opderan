import { ComponentFixture, TestBed } from '@angular/core/testing';
import { CetakorderanPage } from './cetakorderan.page';

describe('CetakorderanPage', () => {
  let component: CetakorderanPage;
  let fixture: ComponentFixture<CetakorderanPage>;

  beforeEach(async(() => {
    fixture = TestBed.createComponent(CetakorderanPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
