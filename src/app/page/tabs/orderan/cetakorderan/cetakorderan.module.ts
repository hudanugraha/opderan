import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CetakorderanPageRoutingModule } from './cetakorderan-routing.module';

import { CetakorderanPage } from './cetakorderan.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CetakorderanPageRoutingModule
  ],
  declarations: [CetakorderanPage]
})
export class CetakorderanPageModule {}
