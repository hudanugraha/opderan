import { Component, OnDestroy, OnInit } from '@angular/core';
import { NavigationExtras, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { Orderan } from 'src/app/models/orderan.model';
import { GlobalService } from 'src/app/services/global/global.service';
import { OrderanService } from 'src/app/services/orderan/orderan.service';
import { App } from '@capacitor/app';


@Component({
  selector: 'app-orderan',
  templateUrl: './orderan.page.html',
  styleUrls: ['./orderan.page.scss'],
})
export class OrderanPage implements OnInit, OnDestroy {
  
  namatoko:any;
  segmen= "akandatang";
  hitung=1;
  namadata="orderan";
  isLoading=false;

  orderanAkanDatang : any = [];
  orderanAkanDatangSub : Subscription;
  orderanLewat : any = [];
  orderanLewatSub : Subscription;
  namaTokoSub:Subscription;
  

  constructor(
    private orderanServis : OrderanService,
    private router: Router,
    private global : GlobalService,
  ) { 
    this.getAppName();
    
  }

  async getAppName() {
    this.namatoko=await this.global.getAppName();
  }

  async ngOnInit() {    
    this.orderanAkanDatangSub = this.orderanServis.orderansakandatang.subscribe(orderan => {
      //console.log("Data Baru ini  : ",pelanggan);
      this.isLoading = true;
      this.orderanAkanDatang = orderan;
      // if (pelanggan instanceof Array) {
      //   this.pelanggans = pelanggan;
      this.isLoading=false;
      // }
    })
    this.orderanLewatSub = this.orderanServis.orderanslewat.subscribe(orderan => {
      //console.log("Data Baru ini  : ",pelanggan);
      this.isLoading = true;
      this.orderanLewat = orderan;
      // if (pelanggan instanceof Array) {
      //   this.pelanggans = pelanggan;
      this.isLoading=false;
      // }
    });

    this.namaTokoSub= this.global.namatoko.subscribe(arrnamatoko => {
      if (arrnamatoko) {
        let arr:any=[]
        arr=arrnamatoko;
        this.namatoko=arr.namatoko;      
      }
    });

    this.getOrderan();
    this.getAppName();
  }

  ngOnDestroy() {
    if (this.orderanAkanDatangSub) this.orderanAkanDatangSub.unsubscribe();
    if (this.orderanLewatSub) this.orderanLewatSub.unsubscribe();
    if (this.namaTokoSub) this.namaTokoSub.unsubscribe();
    
  }

  async getNamaToko() {
    await this.global.getAppName();
  }

  async getOrderan() {
    this.isLoading = true;
    await this.orderanServis.getOrderan(true);     
    this.isLoading=false;
  }

  async gantiSegment(event:any) {
    this.segmen=event.detail.value;
    this.isLoading = true;
    if (this.segmen=="akandatang") {
      await this.orderanServis.getOrderan(true);     
    } else {
      await this.orderanServis.getOrderan(false);
    }
    this.isLoading=false;
  }

  editOrderan(orderan:Orderan) {
    const navData: NavigationExtras = {
      queryParams: {
        data: JSON.stringify(orderan)
      }
    }
    this.router.navigate([this.router.url, 'tambahorderan'],navData);
  }

  async hapusOrderan(orderan:Orderan, tipe='akandatang') {
    this.global.showAlert(
      "Yakin ingin menghapus?",
      "Konfirmasi",
      [
              {
                text : "Tidak",
                role : 'cancel',
                handler: () => {
                  console.log('cancel');
                  return;
                }
              },
              {
                text : "Ya",
                handler : async () => {
                  await this.orderanServis.hapusOrderan(orderan.id,tipe);
                  console.log('YA');
                }
              }
            ]
    )
  }

  navigateToDetail(orderan:Orderan) {
    const navData: NavigationExtras = {
      queryParams: {
        data: JSON.stringify(orderan)
      }
    }
    //this.router.navigate(['tabs','orderan','detailorderan'], navData);
    // this.storage.set('orderansedangaktif',orderan);

    this.router.navigate([this.router.url, 'detailorderan',orderan.id]);
  }

  exitApp() {
    App.exitApp();
  }

}
