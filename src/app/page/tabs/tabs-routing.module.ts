import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TabsPage } from './tabs.page';

const routes: Routes = [
  {
    path: '',
    component: TabsPage,
    children: [
      {
        path: 'produk',
        loadChildren: () => import('./produk/produk/produk.module').then( m => m.ProdukPageModule)
      },
      {
        path: 'orderan',
        loadChildren: () => import('./orderan/orderan/orderan.module').then( m => m.OrderanPageModule)
      },
      {
        path: 'pelanggan',
        loadChildren: () => import('./pelanggan/pelanggan/pelanggan.module').then( m => m.PelangganPageModule)
      },
      {
        path: '',
        redirectTo: '/tabs/orderan',
        pathMatch: 'full'
      }
    ]
  },
  {
    path: '',
    redirectTo: '/tabs/orderan',
    pathMatch: 'full'
  },
  {
    path: 'pelanggan/tambahpelanggan',
    loadChildren: () => import('./pelanggan/tambahpelanggan/tambahpelanggan.module').then( m => m.TambahpelangganPageModule)
  },
  {
    path: 'pelanggan/tambahpelanggan/:data',
    loadChildren: () => import('./pelanggan/tambahpelanggan/tambahpelanggan.module').then( m => m.TambahpelangganPageModule)
  },  
  {
    path: 'produk/tambahproduk',
    loadChildren: () => import('./produk/tambahproduk/tambahproduk.module').then( m => m.TambahprodukPageModule)
  },  
  {
    path: 'produk/tambahproduk/:data',
    loadChildren: () => import('./produk/tambahproduk/tambahproduk.module').then( m => m.TambahprodukPageModule)
  },
  {
    path: 'produk/kategori',
    loadChildren: () => import('./produk/kategori/kategori/kategori.module').then( m => m.KategoriPageModule)
  },
  {
    path: 'produk/kategori/tambahkategori',
    loadChildren: () => import('./produk/kategori/tambahkategori/tambahkategori.module').then( m => m.TambahkategoriPageModule)
  },
  {
    path: 'produk/kategori/tambahkategori/:data',
    loadChildren: () => import('./produk/kategori/tambahkategori/tambahkategori.module').then( m => m.TambahkategoriPageModule)
  },
  {
    path: 'orderan/tambahorderan',
    loadChildren: () => import('./orderan/tambahorderan/tambahorderan.module').then( m => m.TambahorderanPageModule) 
  },
  {
    path: 'orderan/tambahorderan/:data',
    loadChildren: () => import('./orderan/tambahorderan/tambahorderan.module').then( m => m.TambahorderanPageModule) 
  },
  {
    path: 'orderan/detailorderan',
    loadChildren: () => import('./orderan/detailorderan/detailorderan/detailorderan.module').then( m => m.DetailorderanPageModule)
  },
  {
    path: 'orderan/detailorderan/:id',
    loadChildren: () => import('./orderan/detailorderan/detailorderan/detailorderan.module').then( m => m.DetailorderanPageModule)
  },
  {
    path: 'orderan/tambahorderanpelanggan/:id',
    loadChildren: () => import('./orderan/detailorderan/tambahorderanpelanggan/tambahorderanpelanggan/tambahorderanpelanggan.module').then( m => m.TambahorderanpelangganPageModule)
  },
  {
    path: 'orderan/tambahorderanproduk/:id_orderan/:id_pelanggan',
    loadChildren: () => import('./orderan/detailorderan/tambahorderanpelanggan/tambahorderanproduk/tambahorderanproduk.module').then( m => m.TambahorderanprodukPageModule)
  },
  {
    path: 'orderan/detailorderan/editorderan/:id_orderan/:id_pelanggan',
    loadChildren: () => import('./orderan/detailorderan/editorderan/editorderan.module').then( m => m.EditorderanPageModule)
  },
  {
    path: 'produk/cetakproduk',
    loadChildren: () => import('./produk/cetakproduk/cetakproduk.module').then( m => m.CetakprodukPageModule)
  },
  {
    path: 'orderan/statistikitem/:orderan_id',
    loadChildren: () => import('./orderan/detailorderan/statistikitem/statistikitem.module').then( m => m.StatistikitemPageModule)
  },
  {
    path: 'orderan/statistikstatusbayar/:orderan_id',
    loadChildren: () => import('./orderan/detailorderan/statistikstatusbayar/statistikstatusbayar.module').then( m => m.StatistikstatusbayarPageModule)
  },
  {
    path: 'orderan/cetakorderan',
    loadChildren: () => import('./orderan/cetakorderan/cetakorderan.module').then( m => m.CetakorderanPageModule)
  },
  {
    path: 'orderan/cetakorderan/:orderan_id',
    loadChildren: () => import('./orderan/cetakorderan/cetakorderan.module').then( m => m.CetakorderanPageModule)
  },
  {
    path: 'orderan/cetakorderan/:orderan_id/:pelanggan_id',
    loadChildren: () => import('./orderan/cetakorderan/cetakorderan.module').then( m => m.CetakorderanPageModule)
  },
  {
    path: 'orderan/setupapp',
    loadChildren: () => import('./orderan/setupapp/setupapp.module').then( m => m.SetupappPageModule)
  }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TabsPageRoutingModule {}
