import { Component, OnInit, ViewChild  } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Toast } from '@capacitor/toast';
import { ActivatedRoute, Router } from '@angular/router';   
import { LoadingController } from '@ionic/angular';
import { StorageService } from 'src/app/services/storage/storage.service';
import { PelangganService } from 'src/app/services/pelanggan/pelanggan.service';
import { GlobalService } from 'src/app/services/global/global.service';
import { Pelanggan } from 'src/app/models/pelanggan.model';


@Component({
  selector: 'app-tambahpelanggan',
  templateUrl: './tambahpelanggan.page.html',
  styleUrls: ['./tambahpelanggan.page.scss'],
})
export class TambahpelangganPage implements OnInit {
  formPelanggan : FormGroup;
  isSubmitted = false;
  isExisted = false;
  phonePattern = /^[0-9]{10,12}$/;
  lagiUpdate : boolean;
  idUpdate : 0;
  pelanggan : any=[];
  isFromOrderanPage = false;
  @ViewChild('namaPelanggan') namaPelanggan:any;

  constructor(
    public formBuilder: FormBuilder, 
    public storage : StorageService,
    private router: Router,
    private loadingCtrl: LoadingController,
    private pelangganService : PelangganService,
    private global : GlobalService,
    private route :ActivatedRoute
  ) { 
    const id=1;
  }

  ngOnInit() {  
    // this.storage.clear();
    // this.storage.remove("pelanggan");
    // return false;

    this.initForm();
    this.checkForUpdate();
  }

  initForm(pelanggan?: any) {
    let data: any = {
      nama : null,
      no_hp : null
    }
    if (pelanggan) {
      data = {
        nama : pelanggan.nama,
        no_hp : pelanggan.no_hp
      }
    }
    this.formPelanggan = this.formBuilder.group({
      nama: [data.nama, [Validators.required, Validators.minLength(4)]],
      no_hp: [data.no_hp, [Validators.required, ]]
    });
  }

  ionViewDidEnter(){
    this.namaPelanggan.setFocus();
  }

  get errorControl() {
    return this.formPelanggan.controls;
  }
  
  async submitForm() {
    this.global.showLoader("Menyimpan...");
    this.isSubmitted = true;
    if (!this.formPelanggan.valid) {
      console.log('Please provide all the required values!')
      this.global.hideLoader();
    } else {
      //console.log(this.formPelanggan.value);
      if (this.lagiUpdate) {
        await this.pelangganService.updatePelanggan(this.idUpdate,this.formPelanggan.value);
      } else {
        await this.pelangganService.tambahPelanggan(this.formPelanggan.value);      
      }
      if (!this.isFromOrderanPage) {
        this.router.navigate(['tabs/pelanggan']);
      } else {
        this.router.navigate(['tabs/orderan/tambahorderanpelanggan/'+this.idUpdate]);
      }        
      this.formPelanggan.reset(); 
      this.global.hideLoader();
    }
  }

  checkForUpdate() {
    // karena halaman edit dan tambah sama, maka fungsi ini diperlukan utnuk mengecek apakah sekarang ini sedang edit atau tambah
    this.route.queryParams.subscribe(async(data) => {
      if (data?.['data']) {
        const pelanggan = JSON.parse(data['data']);
        this.lagiUpdate=true;
        this.idUpdate = pelanggan.id;
        this.pelanggan.nama=pelanggan.nama;
        this.pelanggan.no_hp=pelanggan.no_hp;
        if (pelanggan.isFromOrderanPage) {
          this.isFromOrderanPage=true;
        }
        this.initForm(this.pelanggan);
      } else {
        this.lagiUpdate=false;
      }
    })
  }

}
