import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute,  NavigationExtras,  Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { Pelanggan } from 'src/app/models/pelanggan.model';
import { GlobalService } from 'src/app/services/global/global.service';
import { PelangganService } from 'src/app/services/pelanggan/pelanggan.service';
import { SqlService } from 'src/app/services/sql/sql.service';
import { StorageService } from 'src/app/services/storage/storage.service';


@Component({
  selector: 'app-pelanggan',
  templateUrl: './pelanggan.page.html',
  styleUrls: ['./pelanggan.page.scss'],
})
export class PelangganPage implements OnInit, OnDestroy {

  pelanggans : any = [];
  pelanggansSub : Subscription;
  KEY = "pelanggan";
  jumlah_pelanggan = 0;
  isLoading : boolean;
  queryCari : string ="";
  
  constructor(
    public storage : StorageService,
    private router: Router,
    private pelangganService : PelangganService,
    private global : GlobalService,
    private sql:SqlService
  ) { 
    
  }


  async ngOnInit() {
    this.pelanggansSub = this.pelangganService.pelanggans.subscribe(pelanggan => {
      //console.log("Data Baru ini  : ",pelanggan);
      this.isLoading = true;
      this.pelanggans = pelanggan;
      this.jumlah_pelanggan=pelanggan.length;
      this.isLoading=false;
      // }
    })
    this.getPelanggan();

    //  // this.sql.setQuery("INSERT INTO pelanggan VALUES (1,'HUDA','085751014949')");
    //   let hasil=await this.sql.getQuery("SELECT * FROM pelanggan");
    //   console.log("HASIL", hasil);
  }

  
  async getPelanggan() {
    // this.isLoading = true;
    await this.pelangganService.getPelanggan();     
    // this.isLoading=false;
  }
  
  ngOnDestroy() {
    if (this.pelanggansSub) this.pelanggansSub.unsubscribe();
  }

  async hapusPelanggan(pelanggan:Pelanggan) {
    this.global.showAlert(
      "Yakin ingin menghapus?",
      "Konfirmasi",
      [
              {
                text : "Tidak",
                role : 'cancel',
                handler: () => {
                  console.log('cancel');
                  return;
                }
              },
              {
                text : "Ya",
                handler : async () => {
                  this.global.showLoader("Menghapus...");
                  await this.pelangganService.hapusPelanggan(pelanggan.id);
                  this.global.hideLoader();
                }
              }
            ]
    )
  }

  editPelanggan(pelanggan:Pelanggan) {
    const navData: NavigationExtras = {
      queryParams: {
        data: JSON.stringify(pelanggan)
      }
    }
    this.router.navigate([this.router.url, 'tambahpelanggan'],navData);
  }

  async cariPelanggan(event: any) {
    // this.queryCari = event.detail.value.toLowerCase();
    // const temp = await this.storage.get("pelanggan");
    // let allPelanggan: Pelanggan [] =  JSON.parse(temp.value);
    // console.log("allPelanggan", allPelanggan);
    // if (this.queryCari.length > 0) {
    //   console.log("ADA QUERY");
    //   setTimeout(async() => {
    //     this.pelanggans = await allPelanggan.filter((element:any) => {
    //       return element.nama.toLowerCase().includes(this.queryCari);
    //     })
    //     this.jumlah_pelanggan=this.pelanggans.length;
    //   });
    // } else {
    //   console.log("TIDAK ADA QUERY");
    //   this.getPelanggan();
    // }

    this.queryCari = event.detail.value.toLowerCase();
    this.global.showLoader("Mencari...");
    if (this.queryCari.length > 0) { 
      this.pelangganService.cariPelanggan(this.queryCari);
    } else {
      this.getPelanggan();
    }
    this.global.hideLoader();
    
  }

  /////////////////////////////////////////////
  // async ngOnInit() {
  //   // this.pelanggansSub = this.pelangganService.pelanggans.subscribe(pelanggans => {
  //   //   console.log("Pelanggan NEW : ",pelanggans);
  //   //   this.pelanggans = pelanggans;
  //   //   if (pelanggans instanceof Array) {
  //   //     this.pelanggans = pelanggans;
  //   //   } else {
  //   //     // if (pelanggans?.delete) {
  //   //     //   this.pelanggans = this.pelanggans.filter(x => x.no_hp != pelanggans.no_hp)
  //   //     // } else if (pelanggans?.update) {
  //   //     //   //const index = this.pelangganService/.
  //   //     //   //
  //   //     // } else {
  //   //     //   this.pelanggans = this.pelanggans.concat(pelanggans);
  //   //     // }
  //   //   }
  //   // })


  //   // const data= await this.pelangganService.getAllPelanggan();
  //   // console.log("Data awal", data);
  //   // this.pelanggans = data;   

  //     this.pelanggansSub = this.pelangganService.pelanggans.subscribe((data:any) => {
  //       this.pelanggans = data;  
  //   });
  // }



  // async getPelanggan() {
  //    this.pelangganService.getAllPelanggan();
  // }

  // deletePelanggan(pelanggan:any) {
  //   console.log("Pelanggan Asal ",pelanggan);
  //   this.global.showAlert(
  //     'Yakin ingin menghapus?',
  //     'Konfirmasi',
  //     [
  //       {
  //         text : "Tidak",
  //         role : 'cancel',
  //         handler: () => {
  //           console.log('cancel');
  //           return;
  //         }
  //       },
  //       {
  //         text : "Ya",
  //         handler : async () => {
  //           this.global.showLoader();
  //           await this.pelangganService.hapusPelanggan(pelanggan);
  //         }
  //       }
  //     ]
  //   )
  // }
 
  //////////////////////////////////////////////// 
  
}
