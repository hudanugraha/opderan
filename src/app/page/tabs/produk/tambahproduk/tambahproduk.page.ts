import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { Produk } from 'src/app/models/produk.model';
import { GlobalService } from 'src/app/services/global/global.service';
import { KategoriService } from 'src/app/services/kategori/kategori.service';
import { ProdukService } from 'src/app/services/produk/produk.service';
import { StorageService } from 'src/app/services/storage/storage.service';

@Component({
  selector: 'app-tambahproduk',
  templateUrl: './tambahproduk.page.html',
  styleUrls: ['./tambahproduk.page.scss'],
})
export class TambahprodukPage implements OnInit, OnDestroy {
  formProduk : FormGroup<any>;
  kategoris : any = [];
  kategorisSub : Subscription;
  isLoading : boolean;
  isSubmitted = false;
  idUpdate : 0;
  lagiUpdate : boolean;
  idKategoriUntukTambah : number;
  public selectedkategoriid=0;
  @ViewChild('namaProduk') namaProduk:any;
  
  constructor(
    public formBuilder: FormBuilder, 
    public storage : StorageService,
    protected activeRoute: ActivatedRoute,
    private router: Router,
    private kategoriServis : KategoriService,
    private produkServis : ProdukService,
    private global : GlobalService,
    private route :ActivatedRoute,
  ) { }

  ngOnInit() {
    if (this.kategoriServis.kategori) {
      this.kategorisSub = this.kategoriServis.kategori.subscribe(kategori => {
        this.kategoris = kategori;        
      })
    }
    this.getKategori();
    this.initForm();
    this.checkForUpdate();
  }

  initForm(produk?: any) {
    let data: any = {
      nama_produk : null,
      harga_pokok : null,
      harga_jual : null,
      kategori_id : null,
      tampilkan : true
    }
    if (produk) {
      data = {
        nama_produk : produk.nama_produk,
        harga_pokok : produk.harga_pokok,
        harga_jual : produk.harga_jual,
        kategori_id : produk.kategori_id,
        tampilkan : produk.tampilkan
      }
      this.selectedkategoriid=produk.kategori_id;
    }
    this.formProduk = this.formBuilder.group({
      nama_produk: [data.nama_produk, [Validators.required, Validators.minLength(3)]],
      harga_pokok: [data.harga_pokok, [Validators.required, ]],
      harga_jual: [data.harga_jual, [Validators.required, ]],
      kategori_id: [data.kategori_id, [Validators.required, ]],
      tampilkan:[data.tampilkan]
    });

  }

  async getKategori() {
    this.isLoading = true;
    await this.kategoriServis.getKategori();     
    this.isLoading=false;
  }

  ngOnDestroy() {
    if (this.kategorisSub) this.kategorisSub.unsubscribe();
  }

  get errorControl() {
    return this.formProduk.controls;
  }

  ionViewDidEnter(){
    this.namaProduk.setFocus();
  }

  checkForUpdate() {
    // karena halaman edit dan tambah sama, maka fungsi ini diperlukan utnuk mengecek apakah sekarang ini sedang edit atau tambah
   this.route.queryParams.subscribe(async(data) => {
     if (data?.['data']) {
       const produk = JSON.parse(data['data']);
       this.lagiUpdate=true;
       this.idUpdate = produk.id;
       this.initForm(produk);
     } else {
       this.lagiUpdate=false;
     }
   })
 }

  async submitForm() {
    // this.formProduk.value.kategori_id=Number(this.idKategoriUntukTambah);
    // this.formProduk.value.kategori_id=this.idKategoriUntukTambah;
    this.global.showLoader("Menyimpan...");
    this.isSubmitted = true;
    if (!this.formProduk.valid) {
      console.log('Please provide all the required values!')
      this.global.hideLoader();
    } else {
      //console.log(this.formPelanggan.value);
      if (this.lagiUpdate) {
        await this.produkServis.updateProduk(this.idUpdate,this.formProduk.value);
      } else {
        await this.produkServis.tambahProduk(this.formProduk.value);      
      }
      this.router.navigate(['tabs/produk']);        
      this.formProduk.reset(); 
      this.global.hideLoader();
    }
  }
  
  setIdKategori(event:any) {
    console.log("EVENT", event.detail.value);
    this.idKategoriUntukTambah=event.detail.value;
  }

}
