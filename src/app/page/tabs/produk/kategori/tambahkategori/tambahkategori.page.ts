import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators, } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GlobalService } from 'src/app/services/global/global.service';
import { KategoriService } from 'src/app/services/kategori/kategori.service';
import { StorageService } from 'src/app/services/storage/storage.service';

@Component({
  selector: 'app-tambahkategori',
  templateUrl: './tambahkategori.page.html',
  styleUrls: ['./tambahkategori.page.scss'],
})
export class TambahkategoriPage implements OnInit { 
  formKategori : FormGroup;
  lagiUpdate = false;
  isSubmitted = false;
  idUpdate : 0;
  kategori : any=[];
  
  @ViewChild('namaKategori') namaKategori:any;

  constructor(
    public formBuilder: FormBuilder, 
    private global : GlobalService,
    private storage : StorageService,
    private kategoriService : KategoriService,
    private router: Router,
    private route :ActivatedRoute,
    
  ) {
    const id=1;
   }

  ngOnInit() {
    this.initForm();
    this.checkForUpdate();
  }

  initForm(kategori?: any) {
    let data: any = {
      nama_kategori : null
    }
    if (kategori) {
      data = {
        nama_kategori : kategori.nama_kategori
      }
    }
    this.formKategori = this.formBuilder.group({
      nama_kategori : [data.nama_kategori, [Validators.required, Validators.minLength(4)]],
    });
  }

  get errorControl() {
    return this.formKategori.controls;
  }

  ionViewDidEnter(){
   this.namaKategori.setFocus();
   
  }

  checkForUpdate() {
     // karena halaman edit dan tambah sama, maka fungsi ini diperlukan utnuk mengecek apakah sekarang ini sedang edit atau tambah
    this.route.queryParams.subscribe(async(data) => {
      if (data?.['data']) {
        const kategori = JSON.parse(data['data']);
        this.lagiUpdate=true;
        this.idUpdate = kategori.id;
        this.kategori.nama_kategori=kategori.nama_kategori;
        this.initForm(this.kategori);
      } else {
        this.lagiUpdate=false;
      }
    })
  }

  async submitForm() {
    this.global.showLoader("Menyimpan...");
    this.isSubmitted = true;
    if (!this.formKategori.valid) {
      console.log('Please provide all the required values!')
      this.global.hideLoader();
    } else {
      //console.log(this.formPelanggan.value);
      if (this.lagiUpdate) {
        await this.kategoriService.updateKategori(this.idUpdate,this.formKategori.value);
      } else {
        await this.kategoriService.tambahKategori(this.formKategori.value);      
      }
      
      this.router.navigate(['tabs/produk/kategori']);  
      this.formKategori.reset();
      this.global.hideLoader();
    }
  }

}
