import { ComponentFixture, TestBed } from '@angular/core/testing';
import { TambahkategoriPage } from './tambahkategori.page';

describe('TambahkategoriPage', () => {
  let component: TambahkategoriPage;
  let fixture: ComponentFixture<TambahkategoriPage>;

  beforeEach(async(() => {
    fixture = TestBed.createComponent(TambahkategoriPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
