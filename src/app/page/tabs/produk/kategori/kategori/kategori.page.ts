import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, NavigationExtras, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { Kategori } from 'src/app/models/kategori.model';
import { Produk } from 'src/app/models/produk.model';
import { GlobalService } from 'src/app/services/global/global.service';
import { KategoriService } from 'src/app/services/kategori/kategori.service';
import { StorageService } from 'src/app/services/storage/storage.service';


@Component({
  selector: 'app-kategori',
  templateUrl: './kategori.page.html',
  styleUrls: ['./kategori.page.scss'],
})
export class KategoriPage implements OnInit, OnDestroy {

  kategoris : any = [];
  kategorisSub : Subscription;
  jumlah_kategori = 0;
  isLoading : boolean;
  
  constructor(
    public storage : StorageService,
    protected activeRoute: ActivatedRoute,
    private router: Router,
    private kategoriServis : KategoriService,
    private global : GlobalService,
  ) {  }

  ngOnInit() {
    if (this.kategoriServis.kategori) {
      this.kategorisSub = this.kategoriServis.kategori.subscribe(kategori => {
        this.isLoading = true;
        this.kategoris = kategori;
        this.jumlah_kategori=kategori.length;
        // if (pelanggan instanceof Array) {
        //   this.pelanggans = pelanggan;
        this.isLoading=false;
        // }
      })
    }
    this.getKategori();
  }

  ngOnDestroy() {
    if (this.kategorisSub) this.kategorisSub.unsubscribe();
  }

  

  async getKategori() {
    this.isLoading = true;
    await this.kategoriServis.getKategori();     
    this.isLoading=false;
  }

  async hapusKategori(kategori:Kategori) {
    // if (this.cekExistedKategori(kategori.id)==0) {
      this.global.showAlert(
        "Yakin ingin menghapus?",
        "Konfirmasi",
        [
                {
                  text : "Tidak",
                  role : 'cancel',
                  handler: () => {
                    console.log('cancel');
                    return;
                  }
                },
                {
                  text : "Ya",
                  handler : async () => {
                    this.global.showLoader("Menghapus...");
                    await this.kategoriServis.hapusKategori(kategori.id);
                    this.global.hideLoader();
                  }
                }
              ]
      )
    // } else {
    //   this.global.showAlert("Gagal menghapus, data sudah digunakan");
    // }
  }

  editKategori(kategori:Kategori) {
    const navData: NavigationExtras = {
      queryParams: {
        data: JSON.stringify(kategori)
      }
    }
    this.router.navigate([this.router.url, 'tambahkategori'],navData);
  }
  
  async cekExistedKategoriinProduk(id:number) {
    // const data=await this.storage.get("produk");
    // let allProduk: Produk [] =  JSON.parse(data.value);
    // const hasilcek = allProduk.findIndex((x:any) => {
    //     return Number(x.id) === id;
    // });
    // return hasilcek;
    // const hasilcek = this._produks.findIndex((x:any) => {
    //   return Number(x.id) === id;
    // })
    // // if (hasilcek===1) {
    // //   return true;
    // // } else {
    // //   return false;
    // // }
    // return hasilcek;
  }


}
