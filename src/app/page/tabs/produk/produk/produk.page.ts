import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, NavigationExtras, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { Produk } from 'src/app/models/produk.model';
import { GlobalService } from 'src/app/services/global/global.service';
import { KategoriService } from 'src/app/services/kategori/kategori.service';
import { ProdukService } from 'src/app/services/produk/produk.service';
import { StorageService } from 'src/app/services/storage/storage.service';


@Component({
  selector: 'app-produk',
  templateUrl: './produk.page.html',
  styleUrls: ['./produk.page.scss'],
})
export class ProdukPage implements OnInit, OnDestroy {

  kategoris : any = [];
  kategorisSub : Subscription; 
  produks : any = [];
  produksSub : Subscription; 
  isLoading : boolean;
  jumlah_produk = 0;
  queryCari = "";
  filterKategoriProduk=0;
  
  constructor(
    private kategoriServis : KategoriService,
    public storage : StorageService,
    protected activeRoute: ActivatedRoute,
    private router: Router,
    private global : GlobalService,
    private produkServis : ProdukService
  ) { }

  ngOnInit() {
    this.kategorisSub = this.kategoriServis.kategori.subscribe(kategori => {
      this.isLoading = true;
      this.kategoris = kategori;
      // if (pelanggan instanceof Array) {
      //   this.pelanggans = pelanggan;
      this.isLoading=false;
      // }
    })
    this.getKategori();

    //////////////
    this.produksSub = this.produkServis.produks.subscribe(produk => {
      this.isLoading = true;
      this.produks = produk;
      this.jumlah_produk=this.produks.length;
      this.isLoading=false;
      // }
    })
    this.getProduk();
  }

  ngOnDestroy() {
    if (this.kategorisSub) this.kategorisSub.unsubscribe();
    if (this.produksSub) this.produksSub.unsubscribe();
  }

  async getKategori() {
    this.isLoading = true;
    await this.kategoriServis.getKategori();     
    this.isLoading=false;
  }

  async getProduk() {
    this.isLoading = true;
    await this.produkServis.getProduk();     
    this.isLoading=false;
  }
  
  ionViewWillEnter() {
    
  }
  
  

  async updateTampilkanProduk(event:any,id:number) {       
    const statuschecked=event.detail.checked;
    this.produkServis.updateTampilkanProduk(id, statuschecked);
    this.global.showToast("Berhasil diubah","primary","bottom", 500);
  }

  async filterCariProduk(event:any, tipe:string) { 
    // if (tipe=='query') {
    //   this.queryCari=event.detail.value.toLowerCase();
    // }
    // if (tipe=='kategori') {
    //   this.filterKategoriProduk=event.detail.value;
    // }

    // if ((this.queryCari !="") || (this.filterKategoriProduk!=0)) {
    //   const temp = await this.storage.get("produk");
    //   let allProduk: Produk [] =  JSON.parse(temp.value);
    //   this.produks = allProduk.filter((element: any) => {
    //     if ((this.queryCari != "") && (this.filterKategoriProduk == 0)) {
    //       // hanya query
    //       return element.nama_produk.includes(this.queryCari);
    //     } else if ((this.filterKategoriProduk != 0) && (this.queryCari == "")) {
    //       // hanya filter kategori
    //       return element.kategori_id == this.filterKategoriProduk;
    //     } else {
    //       // kedua2nya
    //       return element.nama_produk.includes(this.queryCari) && element.kategori_id == this.filterKategoriProduk;
    //     }
    //   });
    // } else {
    //   this.getProduk();
    // }


    if (tipe=='query') {
      this.queryCari=event.detail.value;
    }
    if (tipe=='kategori') {
       this.filterKategoriProduk=event.detail.value;
    }
    if ((this.queryCari !="") || (this.filterKategoriProduk!=0)) {
      this.produkServis.cariProduk(this.queryCari, this.filterKategoriProduk);
    } else {
      this.getProduk();
    }
  }

  hapusProduk(id:number) {
    this.global.showAlert(
      "Yakin ingin menghapus?"+id,
      "Konfirmasi",
      [
              {
                text : "Tidak",
                role : 'cancel',
                handler: () => {
                  console.log('cancel');
                  return;
                }
              },
              {
                text : "Ya",
                handler : async () => {
                  await this.produkServis.hapusProduk(id);
                }
              }
            ]
    )
  }

  editProduk(produk:Produk) {
    const navData: NavigationExtras = {
      queryParams: {
        data: JSON.stringify(produk)
      }
    }
    this.router.navigate([this.router.url, 'tambahproduk'],navData);
  }

}


