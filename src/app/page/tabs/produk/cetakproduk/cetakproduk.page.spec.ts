import { ComponentFixture, TestBed } from '@angular/core/testing';
import { CetakprodukPage } from './cetakproduk.page';

describe('CetakprodukPage', () => {
  let component: CetakprodukPage;
  let fixture: ComponentFixture<CetakprodukPage>;

  beforeEach(async(() => {
    fixture = TestBed.createComponent(CetakprodukPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
