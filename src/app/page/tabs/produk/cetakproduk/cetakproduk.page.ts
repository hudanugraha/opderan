import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { GlobalService } from 'src/app/services/global/global.service';
import { ProdukService } from 'src/app/services/produk/produk.service';

@Component({
  selector: 'app-cetakproduk',
  templateUrl: './cetakproduk.page.html',
  styleUrls: ['./cetakproduk.page.scss'],
})
export class CetakprodukPage implements OnInit,OnDestroy {

  produkscetak : any = [];
  produksSubCetak : Subscription; 
  isLoading=false;
  namatoko:any;
  
  constructor(
    private produkServis : ProdukService,
    private global:GlobalService
    ) { }

  ngOnInit() {
    //////////////
    this.produksSubCetak = this.produkServis.produkcetak.subscribe(produk => {
      this.isLoading = true;
      this.produkscetak = produk;
      this.isLoading=false;      
    })
    this.getProduk();
    this.getAppName();
  }

  async getAppName() {
    this.namatoko=await this.global.getAppName();
  }

  ngOnDestroy() {
    if (this.produksSubCetak) this.produksSubCetak.unsubscribe();
  }

  async getProduk() {
    this.isLoading = true;
    await this.produkServis.getProduk(true,"","cetak");     
    this.isLoading=false;
  }

}
