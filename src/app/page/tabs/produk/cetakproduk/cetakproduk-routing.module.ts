import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CetakprodukPage } from './cetakproduk.page';

const routes: Routes = [
  {
    path: '',
    component: CetakprodukPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CetakprodukPageRoutingModule {}
