import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CetakprodukPageRoutingModule } from './cetakproduk-routing.module';

import { CetakprodukPage } from './cetakproduk.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CetakprodukPageRoutingModule
  ],
  declarations: [CetakprodukPage]
})
export class CetakprodukPageModule {}
