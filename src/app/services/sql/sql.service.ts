import { Injectable } from '@angular/core';
import { SQLite, SQLiteObject } from '@awesome-cordova-plugins/sqlite/ngx';

@Injectable({
  providedIn: 'root'
})
export class SqlService {
  
  public db: SQLiteObject

  constructor(
    public sqlite: SQLite,
  ) { 

     this.createDb();
  }

  async createDb() {
    await this.sqlite.create({
        name: 'dataopderan.db',
        location: 'default'
      })
      .then((db: SQLiteObject) => {
          this.db= db;
      });
  }

  async setQuery(query:string) {
    // await this.createDb();

    // this.db.transaction((tx) => {
    //   tx.executeSql(query, [], (tx:any, res:any) => {
    //     console.log("SUCCESS EXECUTE",res);
    //     return res;
    //   }, (tx:any, err:any) => {
    //     console.log("ERROR EXECUTE",err);
    //   });      
    // })
    this.db.executeSql(query, []).then(() => console.log('Executed SQL',query))
    .catch(e => console.log(e));;
  }

  async createTable(query:string) {
    this.createDb().then(res=>{
      this.db.executeSql(query, [])
        .then(() => {
            // console.log('Executed SQL',query)
        })
        .catch(e => console.log(e));
    });
    
  }

  async getQuery(query:string): Promise<any>  {

    return new Promise((resolve, reject) => {
        let items:any=[];
        this.createDb().then(res=>{
          this.db.executeSql(query,[])
          .then((data) => {
            // console.log("EXECUTED SQL ",query);
            for (let i = 0; i < data.rows.length; i++) {
              let item = data.rows.item(i);
              // do something with it
              items.push(item);
            }
            resolve(items);
          })
          .catch(e => {
            console.log(e);
          })
        })
    });
    
  }

}