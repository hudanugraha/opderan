import { Injectable } from '@angular/core';
import { AlertButton, AlertController, LoadingController, ModalController, ToastController } from '@ionic/angular';
import { SqlService } from '../sql/sql.service';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})

export class GlobalService {

  isLoading : boolean = false; 
  private _namatoko= new BehaviorSubject<[]>([]);

  constructor(
    private alertCtrl: AlertController,
    private toastCtrl : ToastController,
    private loadingCtrl : LoadingController,
    private modalCtrl: ModalController,
    private sql:SqlService
  ) { }

  get namatoko() {
    return this._namatoko.asObservable();
  }

  async showAlert(message:string, header? :any , buttonArray?: (string | AlertButton)[] | undefined ) {
    await this.alertCtrl.create({
      header: header ? header : "Authentication failed",
      message : message,
      buttons: buttonArray ? buttonArray : ["Okay"]
    }).then(alertEl => alertEl.present());
  }

  async showToast(msg:any, color:any, position:any, duration = 3000) {
    const toast = await this.toastCtrl.create({
      message: msg,
      duration: duration,
      color: color,
      position: position,
    });
    await toast.present();
  }

  errorToast(msg:string) {
    this.showToast(
       msg ? msg : 'No Internet Connection;',
      'danger',
      'bottom'
    )
  }

  async showLoader(msg? : string) {
    this.isLoading = true;
    return this.loadingCtrl.create({
      message: msg ? msg : '',
      spinner:  "bubbles"
    }).then(res => {
      res.present().then(()=> {
        if (!this.isLoading) {
          res.dismiss().then(()=>{
            console.log('abort presenting');
          })
        }
      })
    }
    )
    .catch(e => {
      console.log('show loading error: ',e)
    })
  }

  hideLoader() {
      this.isLoading=false;
      return this.loadingCtrl.dismiss()
      .then(() => console.log('dismissed loader'))
      .catch(e => console.log('error hide loader', e));    
  }

  async createModal(options:any) {
    const modal = await this.modalCtrl.create(options);
    await modal.present();
    const {data} = await modal.onWillDismiss();
    console.log("DATA MODAL", data);
    if (data) return data;
  } 

  modalDismiss(val? : any) {
    let data : any = val ? val : null;
    console.log('data', data);
    this.modalCtrl.dismiss(data);
  }

  async getAppName() {
    let q="SELECT `value` FROM appconfig WHERE nama_config='app_name'";
    let hasil=await this.sql.getQuery(q);  
    
    let arrnamatoko:any=[];
    if (hasil.length>0) {
      arrnamatoko.namatoko=hasil[0].value;
    } else {
      arrnamatoko.namatoko="OPDERANAPP";
    }
    // let arrnamatoko['namatoko']=namatoko;
    this._namatoko.next(arrnamatoko);  
    return arrnamatoko.namatoko;    
  }

  async setAppName(app_name:String) {
    let q="SELECT `value` FROM appconfig WHERE nama_config='app_name'";
    let hasil=await this.sql.getQuery(q);
    
    if (hasil.length>0) {
      q="UPDATE appconfig SET value='"+app_name+"' WHERE nama_config='app_name' ";
    } else {
      q="INSERT INTO appconfig (nama_config, value) VALUES ('app_name','"+app_name+"') ";
    }
    await this.sql.setQuery(q);

    let arrnamatoko:any=[];
    arrnamatoko.namatoko=app_name;
    this._namatoko.next(arrnamatoko);  

  }

  async getArrStatusBayar() {
    let status=[
      {
        status : 'Belum Bayar',
        warna : 'dark'
      },
      {
        status : 'BPD Kalsel',
        warna : 'success'
      },
      {
        status : 'BRI',
        warna : 'success'
      },
      {
        status : 'Mandiri',
        warna : 'success'
      },
      {
        status : 'BNI',
        warna : 'success'
      },
      {
        status : 'Bayar Cash',
        warna : 'danger'
      },
      {
        status : 'Talangan Kurir',
        warna : 'primary'
      }
    ]
    return status;
  }
}
