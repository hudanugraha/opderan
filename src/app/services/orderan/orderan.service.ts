import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Orderan } from 'src/app/models/orderan.model';
import { ApiService } from '../api/api/api.service';
import { StorageService } from '../storage/storage.service';
import { ProdukService } from '../produk/produk.service';
import { Produk } from 'src/app/models/produk.model';
import { SqlService } from '../sql/sql.service';


@Injectable({
  providedIn: 'root'
})
export class OrderanService {
  
  DECIMAL_SEPARATOR=",";
  GROUP_SEPARATOR=".";
  private _orderanslewat = new BehaviorSubject<Orderan[]>([]);
  private _orderansakandatang = new BehaviorSubject<Orderan[]>([]);
  private _pelangganorder = new BehaviorSubject<[]>([]);
  private _produkorderanpelanggan = new BehaviorSubject<[]>([]);
  private _statistikstatusbayarjumlah = new BehaviorSubject<[]>([]);
  private _statistikstatusbayarsubtotal = new BehaviorSubject<[]>([]);
  private produks : Produk;
  public statusOrderan=[
    'Belum Bayar', 'BPD Kalsel', 'BRI', 'Mandiri', 'BNI', 'Bayar Cash', 'Talangan Kurir'
  ];
  public arrStatistikSubtotal:any=[];
  public totaltemp=0;
  
  get orderanslewat() {
    return this._orderanslewat.asObservable();
  }
  get orderansakandatang() {
    return this._orderansakandatang.asObservable();
  }
  get pelangganorder() {
    return this._pelangganorder.asObservable();
  }
  get produkorderanpelanggan() {
    return this._produkorderanpelanggan.asObservable();
  }
  get statistikstatusbayarjumlah() {
    return this._statistikstatusbayarjumlah.asObservable();
  }

  get statistikstatusbayarsubtotal() {
    return this._statistikstatusbayarsubtotal.asObservable();
  }


  

  constructor(
    private api: ApiService, 
    private storage : StorageService,
    private produkServis : ProdukService,
    private sql :SqlService
  ) { 
  }

  async getOrderan(akandatang:boolean = true) {
    try {
      // const data=await this.storage.get("orderan");
      // let allOrderan: Orderan [] =  JSON.parse(data.value);  
      // if (allOrderan) {
      //   const today = new Date();
      //   if (akandatang==true) {
      //     // yang akan datang
      //     const arrFilter = allOrderan.filter((element:any) => {
      //           if (new Date(element.tanggal.replace(/-/g, "/")) >= today) {
      //             return element;
      //           }
      //     });
      //     this._orderansakandatang.next(arrFilter);
      //   } else {
      //     //yang sudah lewat
      //     const arrFilter = allOrderan.filter((element:any) => {
      //           if (new Date(element.tanggal.replace(/-/g, "/")) < today) {
      //             return element;
      //           }
      //     });
      //     this._orderanslewat.next(arrFilter);
      //   }      
      // }
      

      // if (allOrderan) {
      //   //filter orderan by date now
      //   const today = new Date();
      //   //const filterByExpiration = arr => arr.filter(({ tanggal }) => new Date(tanggal.replace(/-/g, "/")) > today);
      //   const arrFilterLewat = allOrderan.filter((element:any) => {
      //     if (new Date(element.tanggal.replace(/-/g, "/")) < today) {
      //       return element;
      //     }
      //   })
      //   console.log("yang sudah lewat ",arrFilterLewat);
      // } else {        
      // }
      let today = new Date();
      let dd = String(today.getDate()).padStart(2, '0');
      let mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
      let yyyy = today.getFullYear();
      let hariini=yyyy+"-"+mm+"-"+dd;

      let orderan:any;
      if (akandatang==true) {
        let orderan=await this.sql.getQuery("SELECT a.*, strftime('%d/%m/%Y', a.tanggal)  AS tanggal_teks, b.id_orderan FROM orderan a LEFT JOIN (SELECT DISTINCT id_orderan FROM orderandetail WHERE qty>0) b ON a.id=b.id_orderan WHERE a.tanggal >='"+hariini+"' ");
        this._orderansakandatang.next(orderan);          
      } else {
        let orderan=await this.sql.getQuery("SELECT a.*, strftime('%d/%m/%Y', a.tanggal) AS tanggal_teks, b.id_orderan FROM orderan a LEFT JOIN (SELECT DISTINCT id_orderan FROM orderandetail WHERE qty>0) b ON a.id=b.id_orderan WHERE a.tanggal <'"+hariini+"' ");
        this._orderanslewat.next(orderan);  

      }
    } catch(e) {
      throw(e);
    }    
  }

  async tambahOrderan( value:any) {    
    // const data=await this.storage.get("orderan");
    // let dataSebelumnya: Orderan [] =  JSON.parse(data.value);
    // // // console.log("Data awal",dataSebelumnya);
    // value['id'] = await this.getLastOrderanId();
    // dataSebelumnya.push(value);                 
    // this.storage.set("orderan", dataSebelumnya);
    // this.tambahIdOrderan();
    
    // const today = new Date(); 
    // if (new Date(value.tanggal.replace(/-/g, "/")) >= today) {
    //   const arrFilter = dataSebelumnya.filter((element:any) => {
    //         if (new Date(element.tanggal.replace(/-/g, "/")) >= today) {
    //           return element;
    //         }
    //   });
    //   this._orderansakandatang.next(arrFilter);
    // } else {
    //   const arrFilter = dataSebelumnya.filter((element:any) => {
    //     if (new Date(element.tanggal.replace(/-/g, "/")) < today) {
    //       return element;
    //     }
    //   });
    //   this._orderanslewat.next(arrFilter);
    // }
    // // this._pelanggans.next(dataSebelumnya);
    
    let query="INSERT INTO orderan (lokasi, tanggal) VALUES ('"+value.lokasi+"','"+value.tanggal+"') ";
    this.sql.setQuery(query);
    this.getOrderan(true);
    this.getOrderan(false);
 }

//  async tambahIdOrderan() {
//   let orderan_id : number = await this.getLastOrderanId();
//   orderan_id++;
//   this.storage.set("orderan_id",orderan_id);
// }

// async getLastOrderanId():Promise<number> {
//   const orderan_id=await this.storage.get('orderan_id');
//   if (orderan_id.value==null || orderan_id == undefined) {      
//     return 1;
//   } else {
//     return Number(orderan_id.value);
//   }
// }

async hapusOrderan(id:number,tipe='akandatang') { 
  // const data=await this.storage.get("orderan");
  // let dataSebelumnya: Orderan [] =  JSON.parse(data.value);
  // dataSebelumnya = dataSebelumnya.filter(x => x.id != id);
  // this.storage.set("orderan",dataSebelumnya);
  // if (tipe=='akandatang') {
  //   let arrNext =this._orderansakandatang.value;
  //   arrNext = arrNext.filter(x => x.id != id);
  //   this._orderansakandatang.next(arrNext);
  // } else {
  //   let arrNext =this._orderanslewat.value;
  //   arrNext = arrNext.filter(x => x.id != id);
  //   this._orderanslewat.next(arrNext);
  // }

  let query="DELETE FROM orderan WHERE id='"+id+"' ";
  this.sql.setQuery(query);
  this.getOrderan(true);
    this.getOrderan(false);
}

async updateOrderan(id:number, value:Orderan) {
  // value.id = id;
  // const data=await this.storage.get("orderan");
  // let dataSebelumnya: Orderan [] =  JSON.parse(data.value);
  // const index = dataSebelumnya.findIndex(x => x.id == id);
  // dataSebelumnya[index] = value;
  // this.storage.set("orderan",dataSebelumnya);
  // // komparasi tanggal baru 
  // const today = new Date();
  // await this.getOrderan(true);
  // await this.getOrderan(false);

  let query="UPDATE orderan SET lokasi='"+value.lokasi+"', tanggal='"+value.tanggal+"' WHERE id='"+id+"' ";
  this.sql.setQuery(query);
  this.getOrderan(true);
  this.getOrderan(false);
}


// BAGIAN DETAIL 

async getPelangganOrder(orderan_id:number, queryCari:string="", filterStatus:string="", pelanggan_id:number=0 ) {
  try {
      // untuk filter
      let qcaripelanggan="";
      if (queryCari!="") {
        qcaripelanggan=" AND b.nama LIKE '%"+queryCari+"%'";
      }

      let tipeJoin="LEFT";
      let qstatusbayar="";
      let qcaribelumbayar="";
      if (filterStatus!="") {
        if (filterStatus=="Belum Bayar") {
          qcaribelumbayar=" AND (c.status_bayar IS NULL OR c.status_bayar='Belum Bayar') ";
        } else {
          qstatusbayar=" AND status_bayar='"+filterStatus+"' ";
          tipeJoin="INNER";
        }
      } 
      let qfilteridpelanggan="";
      if (pelanggan_id!=0) {
        qfilteridpelanggan=" AND a.id_pelanggan='"+pelanggan_id+"'";
      }

      // get pelanggan yang order + filternya jika ada
      let q="SELECT DISTINCT a.id_pelanggan, b.nama, c.status_bayar  FROM orderandetail a "+
            "INNER JOIN pelanggan b ON a.id_pelanggan=b.id "+
            tipeJoin+" JOIN (SELECT id_pelanggan, status_bayar FROM statusbayarorderan WHERE id_orderan='"+orderan_id+"' "+qstatusbayar+") c ON a.id_pelanggan=c.id_pelanggan "+
            "WHERE id_orderan='"+orderan_id+"' "+qcaripelanggan+" "+qcaribelumbayar+" "+qfilteridpelanggan;
      
      let pelangganorder=await this.sql.getQuery(q);
      // get item2nya
      pelangganorder.forEach(async (element:any, index:number, array=[]) => { 
        let total=0
        let q2="SELECT a.id_produk, a.harga, a.qty, b.nama_produk, (a.harga * a.qty) AS subtotal,  replace(printf(\"%,d\", (a.harga * a.qty)), ',', '.') AS subtotal_teks ,replace(printf(\"%,d\", a.harga), ',', '.') AS harga_teks FROM orderandetail a INNER JOIN produk b ON a.id_produk=b.id WHERE a.id_orderan='"+orderan_id+"' AND a.id_pelanggan='"+element.id_pelanggan+"' ";
        let itemnya=await this.sql.getQuery(q2);
        // jumlahkan subtotal
        pelangganorder[index].item=itemnya;

        itemnya.forEach((el:any, ind:number, [])=> {
          total=total+(el.harga*el.qty);
        })    
        pelangganorder[index].total_harga=total;
        pelangganorder[index].total_harga_teks=this.formatRibuan(total);

        // hitung jumlah item
        let q3="SELECT SUM(a.qty) AS jumlah_item FROM orderandetail a INNER JOIN produk b ON a.id_produk=b.id WHERE a.id_orderan='"+orderan_id+"' AND a.id_pelanggan='"+element.id_pelanggan+"' ";
        let arrjumlah=await this.sql.getQuery(q3);

        pelangganorder[index].total_item=arrjumlah[0].jumlah_item;
        // total+=subtotal;

        // get warna untuk centang
        let warna_status_bayar=this.getWarnaStatusBayar(element.status_bayar);
        pelangganorder[index].warna_status_bayar=warna_status_bayar;
      });
      this._pelangganorder.next(pelangganorder); 
      return pelangganorder;
  } catch(e) {
      throw(e);
  }     
}

async getProdukOrderanPelanggan(orderan_id:number, pelanggan_id:number) {
  // await this.produkServis.getProduk(true);  
  let q="SELECT a.id, a.nama_produk, a.harga_jual, replace(printf(\"%,d\", a.harga_jual), ',', '.') AS harga_jual_teks, b.qty, b.harga, replace(printf(\"%,d\", (b.qty * b.harga)), ',', '.') AS subtotal FROM produk a "+ 
        "INNER JOIN ("+
        "SELECT * FROM orderandetail "+ 
            "WHERE id_pelanggan='"+pelanggan_id+"' AND id_orderan='"+orderan_id+"' AND qty>0 "+
        ") b "+ 
        " ON a.id=b.id_produk ";
  let produk=await this.sql.getQuery(q);
  this._produkorderanpelanggan.next(produk);
}

hapusDetailProdukPelanggan(id_orderan:number, id_pelanggan:number, id_produk:number) {
  let q="DELETE FROM orderandetail WHERE id_orderan='"+id_orderan+"' AND id_pelanggan='"+id_pelanggan+"' AND id_produk='"+id_produk+"' ";
  this.sql.setQuery(q);
  this.getPelangganOrder(id_orderan);
}

async tambahQty(produk:Produk, orderan_id:number, pelanggan_id:number) {
  // if (this.produks[index].qty) {
  //   this.produks[index].qty++;
  // } else {
  //   this.produks[index].qty=1;
  // }
  // this.hitungTotal();

  // cek if exist record
  let rec=await this.sql.getQuery("SELECT * FROM orderandetail WHERE  id_orderan='"+orderan_id+"' AND id_pelanggan='"+pelanggan_id+"' AND id_produk='"+produk.id+"' ");
  let q="";
  if (rec.length==0) {
    // insert
    q="INSERT INTO orderandetail (id_orderan,id_pelanggan,id_produk,qty,harga) VALUES ('"+orderan_id+"', '"+pelanggan_id+"','"+produk.id+"', 1,'"+produk.harga_jual+"')";        
  } else {
    //tambahkan dan update
    let qty=rec[0].qty;
    qty++;
    
    q="UPDATE orderandetail SET qty='"+qty+"' WHERE id_orderan='"+orderan_id+"' AND id_pelanggan='"+pelanggan_id+"'  AND id_produk='"+produk.id+"'";      
  }
  this.sql.setQuery(q);

  // let qcari=" AND a.nama_produk LIKE '%"+this.queryCari+"%'";  
  // await this.getProduk(qcari);
  // this.hitungTotal();
  // this.sql.setQuery("DELETE FROM orderandetail");
}  

async kurangQty(produk:Produk, orderan_id:number, pelanggan_id:number) {
  let rec=await this.sql.getQuery("SELECT * FROM orderandetail WHERE  id_orderan='"+orderan_id+"' AND id_pelanggan='"+pelanggan_id+"' AND id_produk='"+produk.id+"' ");
  let q="";
  if (rec.length==0) {
    // belum ada data, abaikan saja karena ini pengurangan
  } else {
    // get qty sebelumnya, kurangi jika qty>0
    let qty=rec[0].qty;
    if (qty>0) {  
      qty--;
      q="UPDATE orderandetail SET qty='"+qty+"' WHERE id_orderan='"+orderan_id+"' AND id_pelanggan='"+pelanggan_id+"'  AND id_produk='"+produk.id+"'";   
      this.sql.setQuery(q);

      if (qty==0) {
        // hapus
        q="DELETE FROM orderandetail  WHERE id_orderan='"+orderan_id+"' AND id_pelanggan='"+pelanggan_id+"'  AND id_produk='"+produk.id+"'";   
        this.sql.setQuery(q); 
      }

      // let qcari="";
      // if (this.queryCari!="") { 
      //   qcari=" AND a.nama_produk LIKE '%"+this.queryCari+"%'";  
      // } else {
      //   qcari="";
      // }
      // await this.getProduk(qcari);
    }       
    // this.hitungTotal();
  }    
}

formatRibuan(valString:any) {
  if (!valString) {
      return '';
  }
  let val = valString.toString();
  const parts = this.unFormatRibuan(val).split(this.DECIMAL_SEPARATOR);
  return parts[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, this.GROUP_SEPARATOR)
};

unFormatRibuan(val:any) {
  if (!val) {
      return '';
  }
  val = val.replace(/^0+/, '').replace(/\D/g,'');
  if (this.GROUP_SEPARATOR === ',') {
      return val.replace(/,/g, '');
  } else {
      return val.replace(/\./g, '');
  }
}

async saveStatusBayar(orderan_id:number, id_pelanggan:number, status_bayar:string) {
  // cek if exist
  let rec=await this.sql.getQuery("SELECT * FROM statusbayarorderan WHERE  id_orderan='"+orderan_id+"' AND id_pelanggan='"+id_pelanggan+"'");
  let q="";
  if (rec.length==0) {
    // insert
    q="INSERT INTO statusbayarorderan (id_orderan,id_pelanggan,status_bayar) VALUES ('"+orderan_id+"', '"+id_pelanggan+"','"+status_bayar+"')";        
  } else {
    //update
    q="UPDATE statusbayarorderan SET status_bayar='"+status_bayar+"' WHERE id_orderan='"+orderan_id+"' AND id_pelanggan='"+id_pelanggan+"' ";      
  }
  this.sql.setQuery(q);
}

getStatusOrderan() {
  return this.statusOrderan;
}

getWarnaStatusBayar(status_bayar:string) {
  if ((status_bayar=='BPD Kalsel') || (status_bayar=='BRI') || (status_bayar=='Mandiri') || (status_bayar=='BNI')) {
    return "success";
  } else if (status_bayar=='Bayar Cash') {
    return "danger";
  } else if (status_bayar=='Talangan Kurir') {
    return "primary";
  } else {
    return "danger";
  }
}

async getStatistikStatusBayar(orderan_id:number) {
  let statusbayar=this.getStatusOrderan();
  let arr:any=[];
  let arrqty:any=[];
  let arrsubtotal:any=[];
  for (let i=0; i<statusbayar.length; i++) {
      let pelanggan=await this.getPelangganOrder(orderan_id, "",statusbayar[i]);  
      arrqty.push(pelanggan.length);
      
      // this.arrStatistikSubtotal[i].subtotal=0;
      // totalnya
      

      let subtotal=await this.jumlahkanSubtotalDariStatistikBayar(pelanggan,orderan_id);
      // console.log("SUBTOTAL",subtotal);
      arrsubtotal.push(subtotal);
  }
  
  this._statistikstatusbayarjumlah.next(arrqty);
  this._statistikstatusbayarsubtotal.next(arrsubtotal);
}

countProperties(obj:any) {
  var count = 0;

  for(var prop in obj) {
      if(obj.hasOwnProperty(prop))
          ++count;
  }

  return count;
}

async jumlahkanSubtotalDariStatistikBayar(pelanggan:any,orderan_id:number) {
  let subtotal=0;
  for (let i=0; i<pelanggan.length; i++) {
    // console.log("PELANGGAN",pelanggan[i]);
    let q2="SELECT a.id_produk, a.harga, a.qty, b.nama_produk, (a.harga * a.qty) AS subtotal,  replace(printf(\"%,d\", (a.harga * a.qty)), ',', '.') AS subtotal_teks ,replace(printf(\"%,d\", a.harga), ',', '.') AS harga_teks FROM orderandetail a INNER JOIN produk b ON a.id_produk=b.id WHERE a.id_orderan='"+orderan_id+"' AND a.id_pelanggan='"+pelanggan[i].id_pelanggan+"' ";
    await this.sql.getQuery(q2).then((itemnya) => {
       for (let j=0; j<itemnya.length; j++) {
        subtotal=subtotal+itemnya[j].subtotal;
      }
    });       
  }
  return subtotal;
  // pelanggan.forEach(async (element:any, index:number, array=[]) => { 
  //   //  let arritem=element.item;
  //   let q2="SELECT a.id_produk, a.harga, a.qty, b.nama_produk, (a.harga * a.qty) AS subtotal,  replace(printf(\"%,d\", (a.harga * a.qty)), ',', '.') AS subtotal_teks ,replace(printf(\"%,d\", a.harga), ',', '.') AS harga_teks FROM orderandetail a INNER JOIN produk b ON a.id_produk=b.id WHERE a.id_orderan='"+orderan_id+"' AND a.id_pelanggan='"+element.id_pelanggan+"' ";
  //   // console.log("QYERT", q2);
  //   let itemnya=await this.sql.getQuery(q2);          
  //   itemnya.forEach((el:any, ind:number, [])=> {
  //     // this.arrStatistikSubtotal[i].subtotal=this.arrStatistikSubtotal[i].subtotal+(el.harga*el.qty);
  //     total=total+(el.harga*el.qty);
  //   });
     
  //   console.log("TOTAL",total);  
  // });
  
  // console.log("TOTAL2",total);  
  // return total;
}

}
