import { TestBed } from '@angular/core/testing';

import { OrderanService } from './orderan.service';

describe('OrderanService', () => {
  let service: OrderanService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(OrderanService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
