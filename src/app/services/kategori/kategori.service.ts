import { Injectable, OnDestroy, OnInit } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Kategori } from 'src/app/models/kategori.model';
import { ApiService } from '../api/api/api.service';
import { StorageService } from '../storage/storage.service';
import { SqlService } from '../sql/sql.service';

@Injectable({
  providedIn: 'root'
})
export class KategoriService  {
  subscribe(arg0: (kategori: any) => void): import("rxjs").Subscription {
    throw new Error('Method not implemented.');
  }

  private _kategori = new BehaviorSubject<Kategori[]>([]);
  get kategori() {
    return this._kategori.asObservable();
  }
  
  constructor(private api: ApiService, private storage : StorageService, private sql : SqlService) { 
  }

  async getKategori() {
    try {
      // const data=await this.storage.get("kategori");
      // let allKategori: Kategori [] =  JSON.parse(data.value);
      // if (allKategori) {
      //   this._kategori.next(allKategori);
      // }
     

      let allKategori=await this.sql.getQuery("SELECT a.*, b.kategori_id AS kategori_id_produk FROM kategori a LEFT JOIN (SELECT DISTINCT kategori_id FROM produk) b ON a.id=b.kategori_id ");
      this._kategori.next(allKategori);  
      
    } catch(e) {
      throw(e);
    }  
  }

  async tambahKategori( value:any) {    
  //  const dataSebelumnya =this._kategori.value;
  //  console.log("Data awal",dataSebelumnya);
  //  value['id'] = await this.getLastKategoriId();
  //  dataSebelumnya.push(value);                 
  //  this.storage.set("kategori", dataSebelumnya);
  //  this.tambahIdKategori();
  //  this._kategori.next(dataSebelumnya);    

  // let querys="DROP TABLE kategori";
  // this.sql.setQuery(querys);
  
  
  // let q="SELECT name FROM pragma_table_info('kategori')";
  // let det=await this.sql.getQuery(q);
  // console.log("detail tabel",det);  

  let query="INSERT INTO kategori (nama_kategori) VALUES ('"+value.nama_kategori+"') ";
  this.sql.setQuery(query);
  this.getKategori();
 }

 async hapusKategori(id:number) {
  //  let dataSebelumnya =this._kategori.value;
  //  dataSebelumnya = dataSebelumnya.filter(x => x.id != id);
  //  this.storage.set("kategori",dataSebelumnya);
  //  this._kategori.next(dataSebelumnya);

  let query="DELETE FROM kategori WHERE id='"+id+"' ";
  this.sql.setQuery(query);
  this.getKategori();
 }

 updateKategori(id:number, value:Kategori) {
  //  value.id = id;
  //  const dataSebelumnya =this._kategori.value;
  //  const index = dataSebelumnya.findIndex(x => x.id == id);
  //  dataSebelumnya[index] = value;
  //  this.storage.set("kategori",dataSebelumnya);
  //  this._kategori.next(dataSebelumnya);
  
  let query="UPDATE kategori SET nama_kategori='"+value.nama_kategori+"' WHERE id='"+id+"' ";
  this.sql.setQuery(query);
  this.getKategori();
 }

//  async tambahIdKategori() {
//   let kategori_id : number = await this.getLastKategoriId();
//   kategori_id++;
//   this.storage.set("kategori_id",kategori_id);

// }

// async getLastKategoriId():Promise<number> {
//   const kategori_id=await this.storage.get('kategori_id');
//   if (kategori_id.value==null || kategori_id == undefined) {      
//     return 1;
//   } else {
//     return Number(kategori_id.value);
//   }
// }

}
