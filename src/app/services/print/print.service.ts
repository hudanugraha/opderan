import { Injectable } from '@angular/core'; 
import { BluetoothSerial } from '@awesome-cordova-plugins/bluetooth-serial/ngx';

@Injectable({
  providedIn: 'root'
})
export class PrintService {

  constructor(
    private bluetoothSerial: BluetoothSerial
  ) { }

  async cetak(macAddress:string) {
    await this.bluetoothSerial.clear();
    await this.bluetoothSerial.disconnect();
    let connection = await this.bluetoothSerial.connect(macAddress);
    connection.subscribe(async success => {
    if (success == 'OK') {
        this.bluetoothSerial.write("Test Print").then(() => {
            console.log("Print finished");
            this.bluetoothSerial.disconnect();
          });
        } else {
          this.bluetoothSerial.disconnect();
        }
    })
  }
}
