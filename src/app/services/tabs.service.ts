import { Injectable } from '@angular/core';
import { Observable, Observer } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TabsService {
  private tabChangeObserver: any;
  public tabChange: any;

  constructor() { 
    this.tabChangeObserver = null;
    this.tabChange = new Observable((observer: Observer<string>) => {
        this.tabChangeObserver = observer;
    });
  }

  public changeTabInContainerPage(index: number) {
    console.log("tabChangeObserver ",this.tabChangeObserver);
    //this.tabChangeObserver.next(index);
  }
}
