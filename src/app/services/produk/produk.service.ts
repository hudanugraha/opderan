import { Injectable } from '@angular/core';
import { BehaviorSubject, filter } from 'rxjs';
import { Produk } from 'src/app/models/produk.model';
import { ApiService } from '../api/api/api.service';
import { StorageService } from '../storage/storage.service';
import { Kategori } from 'src/app/models/kategori.model';
import { SqlService } from '../sql/sql.service';


@Injectable({
  providedIn: 'root'
})
export class ProdukService {
  
  DECIMAL_SEPARATOR=",";
  GROUP_SEPARATOR=".";

  private _produks = new BehaviorSubject<Produk[]>([]);
  private _produkscetak = new BehaviorSubject<Produk[]>([]);
  private _produkstatistikitem =new BehaviorSubject<Produk[]>([]);
  private _totalstatistikitem =new BehaviorSubject<[]>([]);
  
  get produks() {
    return this._produks.asObservable();
  }
  get produkstatistikitem() {
    return this._produkstatistikitem.asObservable();
  }
  get totalstatistikitem() {
    return this._totalstatistikitem.asObservable();
  }
  get produkcetak() {
    return this._produkscetak.asObservable();
  }

  constructor(private api: ApiService, private storage : StorageService,private sql:SqlService) { 
  }

  async getProduk(tampilkanonly : boolean = false,queryTambahanUntukFilter:string="",tipe:string="allproduk") {
    try {
      let qtampilkan="";
      if (tampilkanonly==true) {
        if (queryTambahanUntukFilter=="") {
            qtampilkan=" WHERE ";
        } else {
          qtampilkan=" AND ";
        }
        qtampilkan=qtampilkan+" (produk.tampilkan='1' OR produk.tampilkan='true') ";
      }

      let allProduk=await this.sql.getQuery("SELECT kategori.nama_kategori, produk.*, replace(printf(\"%,d\", harga_pokok), ',', '.') AS harga_pokok_teks, replace(printf(\"%,d\", harga_jual), ',', '.') AS harga_jual_teks, orderandetail.id_produk AS id_produk_order FROM produk "+
      " INNER JOIN kategori ON produk.kategori_id=kategori.id  "+
      " LEFT JOIN (SELECT DISTINCT id_produk FROM orderandetail WHERE qty>0) orderandetail ON produk.id=orderandetail.id_produk "
      +queryTambahanUntukFilter+qtampilkan);
      if (tipe=='allproduk') {
        this._produks.next(allProduk); 
      } else {
        this._produkscetak.next(allProduk);
      }
    } catch(e) {
      throw(e);
    }    
  }

  async tambahProduk( value:any) {    
  //   //const dataSebelumnya =await this.storage.get("pelanggan");
  //   const dataSebelumnya =this._produks.value;
  // //  console.log("Data awal",dataSebelumnya);
  //   value['id'] = await this.getLastProdukId();
  //   dataSebelumnya.unshift(value);                 
  //   this.storage.set("produk", dataSebelumnya);
  //   this.tambahIdProduk();
    
  //   // injek nama kategori 
  //   const data=await this.storage.get("kategori");
  //   let allKategori: Kategori [] =  JSON.parse(data.value);
  //   dataSebelumnya.forEach((element, index, array) => {
  //     const hasilcekkategori = allKategori.filter((x:any) => {
  //       return x.id === Number(element.kategori_id);
  //     })
  //     const nama_kategori=hasilcekkategori[0].nama_kategori;
  //     dataSebelumnya[index].nama_kategori=nama_kategori;

  //     // ubah format harga
  //     dataSebelumnya[index].harga_jual_teks=this.formatRibuan(dataSebelumnya[index].harga_jual);
  //     dataSebelumnya[index].harga_pokok_teks=this.formatRibuan(dataSebelumnya[index].harga_pokok);
  //   });

  //   this._produks.next(dataSebelumnya);   
  if (value.tampilkan=="1") {
    value.tampilkan="true";
  } else {
    value.tampilkan="false";
  }
    let query="INSERT INTO produk (nama_produk, harga_pokok, harga_jual, kategori_id, tampilkan) VALUES ('"+value.nama_produk+"',"+value.harga_pokok+","+value.harga_jual+", "+value.kategori_id+", "+value.tampilkan+") ";
    this.sql.setQuery(query);
    this.getProduk(); 
 }

//  async getLastProdukId() {
//     const produk_id=await this.storage.get('produk_id');
//     if (produk_id.value==null || produk_id == undefined) {      
//       return 1;
//     } else {
//       return Number(produk_id.value);
//     }
//  }

//  async tambahIdProduk() {
//     let produk_id : number = await this.getLastProdukId();
//     produk_id++;
//     this.storage.set("produk_id",produk_id);
//  }

 updateTampilkanProduk(id:number, statuschecked:boolean) {
    // const dataSebelumnya =this._produks.value;
    // const index=dataSebelumnya.findIndex((x:any) => {
    //   return Number(x.id) === Number(id);
    // });
    
    // dataSebelumnya[index].tampilkan=statuschecked;
    // this.storage.set("produk", dataSebelumnya);
    // this._produks.next(dataSebelumnya);

    let query="UPDATE produk SET tampilkan='"+statuschecked+"' WHERE id='"+id+"' ";
    this.sql.setQuery(query);
    this.getProduk(); 
 }

 hapusProduk(id:number) {
    // let dataSebelumnya =this._produks.value;
    // dataSebelumnya = dataSebelumnya.filter(x => x.id != id);
    // this.storage.set("produk",dataSebelumnya);
    // this._produks.next(dataSebelumnya);
    
    let query="DELETE FROM produk WHERE id='"+id+"' ";
    this.sql.setQuery(query);
    this.getProduk(); 
 }

 async updateProduk(id:number, value:Produk) {
  // value.id = id;
  // const dataSebelumnya =this._produks.value;
  // const index = dataSebelumnya.findIndex(x => x.id == id);
  // dataSebelumnya[index] = value;
  // this.storage.set("produk",dataSebelumnya);

  //  // injek nama kategori 
  //  const data=await this.storage.get("kategori");
  //  let allKategori: Kategori [] =  JSON.parse(data.value);
  //  dataSebelumnya.forEach((element, index, array) => {
  //    const hasilcekkategori = allKategori.filter((x:any) => {
  //      return x.id === Number(element.kategori_id);
  //    })
  //    const nama_kategori=hasilcekkategori[0].nama_kategori;
  //    dataSebelumnya[index].nama_kategori=nama_kategori;

  //    // ubah format harga
  //    dataSebelumnya[index].harga_jual_teks=this.formatRibuan(dataSebelumnya[index].harga_jual);
  //    dataSebelumnya[index].harga_pokok_teks=this.formatRibuan(dataSebelumnya[index].harga_pokok);
  //  });

  // this._produks.next(dataSebelumnya);
  let query="UPDATE produk SET nama_produk='"+value.nama_produk+"', harga_pokok='"+value.harga_pokok+"', harga_jual='"+value.harga_jual+"', kategori_id='"+value.kategori_id+"', tampilkan='"+value.tampilkan+"' WHERE id='"+id+"' ";
  
  this.sql.setQuery(query);
  this.getProduk(); 
}

cariProduk(queryCari:string, kategori_id:number, tampilkanonly:boolean =false) {
  let q:string="";
  if ((queryCari != "") && (kategori_id== 0)) {
    // hanya query
    q=" WHERE produk.nama_produk LIKE '%"+queryCari+"%' ";  
  } else if ((kategori_id != 0) && (queryCari == "")) {
    q=" WHERE produk.kategori_id='"+kategori_id+"' ";
  } else {
    // kedua2nya
    q=" WHERE produk.nama_produk LIKE '%"+queryCari+"%' AND produk.kategori_id='"+kategori_id+"' ";
  }
  console.log("querycari",queryCari);
  console.log("kategori_id",kategori_id);
  this.getProduk(tampilkanonly,q);
}

formatRibuan(valString:any) {
  if (!valString) {
      return '';
  }
  let val = valString.toString();
  const parts = this.unFormatRibuan(val).split(this.DECIMAL_SEPARATOR);
  return parts[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, this.GROUP_SEPARATOR)

};

unFormatRibuan(val:any) {
  if (!val) {
      return '';
  }
  val = val.replace(/^0+/, '').replace(/\D/g,'');
  if (this.GROUP_SEPARATOR === ',') {
      return val.replace(/,/g, '');
  } else {
      return val.replace(/\./g, '');
  }
}


async getProdukStatistikItem(id_orderan:number, filterKategori:number=0) {
    try {
      let qFilterKategori="";
      if (filterKategori!=0) {
        qFilterKategori=" AND b.kategori_id='"+filterKategori+"' ";
      }

      let q="SELECT DISTINCT a.id_produk, b.nama_produk, SUM(qty) AS qty_total FROM orderandetail a "+
            "INNER JOIN produk b ON a.id_produk=b.id WHERE id_orderan='"+id_orderan+"' "+qFilterKategori+
            "GROUP BY id_produk ORDER BY qty_total DESC";
            
      let allProduk=await this.sql.getQuery(q);
      this._produkstatistikitem.next(allProduk); 
    } catch(e) {
      throw(e);
    }    
}

async getTotalStatistikItem(id_orderan:number, filterKategori:number=0) {
  try {
    let qFilterKategori="";
    if (filterKategori!=0) {
      qFilterKategori=" AND b.kategori_id='"+filterKategori+"' ";
    }

    let q="SELECT a.*,b.nama_produk FROM orderandetail a "+
          "INNER JOIN produk b ON a.id_produk=b.id "+
          "WHERE id_orderan='"+id_orderan+"' "+qFilterKategori;
    let allProduk=await this.sql.getQuery(q);
    let total:any=[];
    console.log("ALLPROD",allProduk);
    let subtotal=0; 
    let subtotalhargapokok=0;
    allProduk.forEach((element:any, index:any, array:[]) => {
       let t=element.qty * element.harga;
       subtotal+=t;

       let t2=element.qty * element.harga_pokok;
       subtotalhargapokok+=t2;
    });
    total.subtotal=this.formatRibuan(subtotal);
    total.subtotalhargapokok=this.formatRibuan(subtotalhargapokok);
    total.profit=this.formatRibuan(subtotal-subtotalhargapokok);
    this._totalstatistikitem.next(total); 
  } catch(e) {
    throw(e);
  }    
}

}
