import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, last } from 'rxjs';
import { ApiService } from '../api/api/api.service';
import { StorageService } from '../storage/storage.service';
import { Pelanggan } from 'src/app/models/pelanggan.model';
import { SqlService } from '../sql/sql.service';



@Injectable({
  providedIn: 'root'
})
export class PelangganService {
  private _pelanggans = new BehaviorSubject<Pelanggan[]>([]);
  private _pelanggansOderan = new BehaviorSubject<Pelanggan[]>([]);

  get pelanggans() {
    return this._pelanggans.asObservable();
  }

  get pelanggansOrderan() {
    return this._pelanggansOderan.asObservable();
  }

  constructor(private api: ApiService, private storage : StorageService,private sql:SqlService) {
    
  }



  async getPelanggan(queryCari:string="") {
    try {
      // const data=await this.storage.get("pelanggan");
      // let allPelanggan: Pelanggan [] =  JSON.parse(data.value);
      // if (allPelanggan) {
      //   this._pelanggans.next(allPelanggan);
      // } else {
        
      // }
      let query="SELECT a.*, b.id_pelanggan AS id_pelanggan_order FROM pelanggan a LEFT JOIN (SELECT DISTINCT id_pelanggan FROM orderandetail WHERE qty>0) b ON a.id=b.id_pelanggan "+queryCari;
      let allPelanggan=await this.sql.getQuery(query);
      this._pelanggans.next(allPelanggan);  
    } catch(e) {
      throw(e);
    }    
  }

  async getPelangganOrderan(orderan_id:number= 0, queryCari:string="") {
    let qOrderanId="";
    if (orderan_id!=0) {
        qOrderanId=" WHERE id_orderan='"+orderan_id+"'";
    }
    let query="SELECT a.*, b.id_pelanggan AS pelanggansudahorder FROM pelanggan a LEFT JOIN ( SELECT DISTINCT id_pelanggan FROM orderandetail WHERE id_orderan='"+orderan_id+"' AND qty>0 ) b ON a.id=b.id_pelanggan "+queryCari;
    let allPelanggan=await this.sql.getQuery(query);
    this._pelanggansOderan.next(allPelanggan); 
  }

  async tambahPelanggan( value:any) {    
     //const dataSebelumnya =await this.storage.get("pelanggan");
    // const dataSebelumnya =this._pelanggans.value;
    // console.log("Data awal",dataSebelumnya);
    // value['id'] = await this.getLastPelangganId();
    // dataSebelumnya.unshift(value);                 
    // this.storage.set("pelanggan", dataSebelumnya);
    // this.tambahIdPelanggan();
    // this._pelanggans.next(dataSebelumnya);    

    let query="INSERT INTO pelanggan (nama,no_hp) VALUES ('"+value.nama+"','"+value.no_hp.toString()+"') ";
    this.sql.setQuery(query);
    this.getPelanggan();

  }

  async hapusPelanggan(id:number) {
    // let dataSebelumnya =this._pelanggans.value;
    // dataSebelumnya = dataSebelumnya.filter(x => x.id != id);
    // this.storage.set("pelanggan",dataSebelumnya);
    // this._pelanggans.next(dataSebelumnya);

    let query="DELETE FROM pelanggan WHERE id='"+id+"' ";
    this.sql.setQuery(query);
    this.getPelanggan();
  }

  async updatePelanggan(id:number, value:Pelanggan) {
    // value.id = id;
    // const dataSebelumnya =this._pelanggans.value;
    // const index = dataSebelumnya.findIndex(x => x.id == id);
    // dataSebelumnya[index] = value;
    // this.storage.set("pelanggan",dataSebelumnya);
    // this._pelanggans.next(dataSebelumnya);

    let query="UPDATE pelanggan SET nama='"+value.nama+"', no_hp='"+value.no_hp+"' WHERE id='"+id+"' ";
    this.sql.setQuery(query);
    this.getPelanggan();
  }

  async cariPelanggan(queryCari:string) {
    
    // let dataSebelumnya=this._pelanggans.value;
    // console.log("data sebelumnya ",dataSebelumnya);
    // let hasilPencarian:any=[];
    // dataSebelumnya = dataSebelumnya.filter((element : any) => {
    //   if (element.nama.includes(queryCari)) {
    //     hasilPencarian.push(element);
    //   }
    // });

    let query=" WHERE nama LIKE '%"+queryCari+"%' ";
    this.getPelanggan(query);    
  }

  async getDetailPelanggan(id:number) {
    let q="SELECT * FROM pelanggan WHERE id='"+id+"' ";
    let pelanggan=await this.sql.getQuery(q);
    if (pelanggan){ 
      return pelanggan[0];
    } else {
      return null;
    }
  }
  // async tambahIdPelanggan() {
  //   let pelanggan_id : number = await this.getLastPelangganId();
  //   pelanggan_id++;
  //   this.storage.set("pelanggan_id",pelanggan_id);

  // }

  // async getLastPelangganId():Promise<number> {
  //   const pelanggan_id=await this.storage.get('pelanggan_id');
  //   if (pelanggan_id.value==null || pelanggan_id == undefined) {      
  //     return 1;
  //   } else {
  //     return Number(pelanggan_id.value);
  //   }
  // }
}
