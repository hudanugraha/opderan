export class Pelanggan {
    constructor(
        public id : number,
        public nama: string,
        public no_hp : string,
        public isFromOrderanPage : boolean = false 
    ){}
}