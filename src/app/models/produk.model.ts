export class Produk {
    constructor(
        public id : number,
        public nama_produk: string,
        public harga_pokok : number,
        public harga_jual : number, 
        public kategori_id : number,
        public tampilkan : boolean,
        public nama_kategori? : string,        
        public harga_pokok_teks? : string,
        public harga_jual_teks? : string, 
        public qty? : number
    ){}
}