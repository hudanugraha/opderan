import { Component, Input, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PopoverController } from '@ionic/angular';
@Component({
  selector: 'app-statusbayar',
  templateUrl: './statusbayar.component.html',
  styleUrls: ['./statusbayar.component.scss'],
})
export class StatusbayarComponent  implements OnInit {

  @Input() statusbayar : any[];
  status=[
    {
      status : 'Belum Bayar',
      warna : 'dark'
    },
    {
      status : 'BPD Kalsel',
      warna : 'success'
    },
    {
      status : 'BRI',
      warna : 'success'
    },
    {
      status : 'Mandiri',
      warna : 'success'
    },
    {
      status : 'BNI',
      warna : 'success'
    },
    {
      status : 'Bayar Cash',
      warna : 'danger'
    },
    {
      status : 'Talangan Kurir',
      warna : 'primary'
    }
  ]
  
  constructor(
    public popoverController: PopoverController
  ) { }

  ngOnInit() {}

  simpan(status: any, warna:any) {
    let data={
      status : status,
      warna : warna
    }
    this.popoverController.dismiss(data);
  }
}
