import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'app.huda.opderan',
  appName: 'Opderan',
  webDir: 'www',
  server: {
    androidScheme: 'https'
  }
};

export default config;
